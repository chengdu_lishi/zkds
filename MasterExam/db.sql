
-- 导出  表 examdb.t_active_code 结构
DROP TABLE IF EXISTS `t_active_code`;
CREATE TABLE IF NOT EXISTS `t_active_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `activePkId` int(11) DEFAULT NULL COMMENT '激活码包ID',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `videoCourseId` int(11) DEFAULT NULL COMMENT '视频课程ID',
  `code` varchar(20) DEFAULT NULL COMMENT '激活码号码',
  `price` decimal(8,2) DEFAULT NULL COMMENT '价格',
  `type` int(1) DEFAULT NULL COMMENT '学习包类型 1题库 2视频 3题库+视频',
  `period` int(6) DEFAULT NULL COMMENT '有效日期，月',
  `activatorId` int(11) DEFAULT NULL COMMENT '激活人ID',
  `active_time` datetime DEFAULT NULL COMMENT '激活时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `courseInfoId` (`courseInfoId`),
  KEY `code` (`code`),
  KEY `videoCourseId` (`videoCourseId`)
) ENGINE=InnoDB AUTO_INCREMENT=5723 DEFAULT CHARSET=utf8 COMMENT='激活码信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_active_package 结构
DROP TABLE IF EXISTS `t_active_package`;
CREATE TABLE IF NOT EXISTS `t_active_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `videoCourseId` int(11) DEFAULT NULL COMMENT '视频课程ID',
  `price` decimal(8,2) DEFAULT NULL COMMENT '激活码单价',
  `number` int(6) DEFAULT NULL COMMENT '激活码数量',
  `type` int(1) DEFAULT NULL COMMENT '激活码类型 1题库 2视频 3题库+视频',
  `period` int(6) DEFAULT NULL COMMENT '有效日期，月',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `courseInfoId` (`courseInfoId`),
  KEY `packageId` (`videoCourseId`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COMMENT='激活码包信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_admin 结构
DROP TABLE IF EXISTS `t_admin`;
CREATE TABLE IF NOT EXISTS `t_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminName` varchar(50) DEFAULT NULL,
  `adminPwd` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `headimg` varchar(250) DEFAULT NULL,
  `is_super` int(1) DEFAULT '0' COMMENT '是否可以查看其它人数据 0否 1是',
  `status` int(1) DEFAULT '1' COMMENT '账户状态 1-正常 2-已删除',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='管理员登录表';

INSERT INTO `t_admin` (`id`, `adminName`, `adminPwd`, `name`, `headimg`, `is_super`, `status`) VALUES (1, 'admin', '649d73ade2935c0ecbe2986055cc6ab6', '系统管理员', 'upload\\admin\\image\\9329c27b-db17-4fa2-9245-bde2b0776a01.jpg', 0, 1);

-- 数据导出被取消选择。
-- 导出  表 examdb.t_admin_role 结构
DROP TABLE IF EXISTS `t_admin_role`;
CREATE TABLE IF NOT EXISTS `t_admin_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adminid` int(11) DEFAULT NULL,
  `roleid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `roleid` (`roleid`),
  KEY `adminid` (`adminid`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8 COMMENT='用户角色对应表';
INSERT INTO `t_admin_role` (`id`, `adminid`, `roleid`) VALUES (184, 1, 1);

-- 数据导出被取消选择。
-- 导出  表 examdb.t_app_version 结构
DROP TABLE IF EXISTS `t_app_version`;
CREATE TABLE IF NOT EXISTS `t_app_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `android_version` int(5) DEFAULT NULL COMMENT 'android版本',
  `android_url` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT 'android下载地址',
  `ios_version` int(5) DEFAULT NULL COMMENT 'ios版本',
  `ios_url` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT 'ios下载地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='app版本信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_banner 结构
DROP TABLE IF EXISTS `t_banner`;
CREATE TABLE IF NOT EXISTS `t_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type` int(1) DEFAULT NULL COMMENT '广告类型 1APP 2网站',
  `scope` int(1) NOT NULL DEFAULT '0' COMMENT '范围 0所有 1一级科目 2二级科目 3三级科目',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程信息ID',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `begin_date` date DEFAULT NULL COMMENT '显示起始日期',
  `end_date` date DEFAULT NULL COMMENT '显示截止日期',
  `image` varchar(120) DEFAULT NULL COMMENT '图片地址',
  `url` varchar(255) DEFAULT NULL COMMENT '链接地址',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `classifyId` (`classifyId`),
  KEY `classifySubId` (`classifySubId`),
  KEY `courseInfoId` (`courseInfoId`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='广告信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_buy_log 结构
DROP TABLE IF EXISTS `t_buy_log`;
CREATE TABLE IF NOT EXISTS `t_buy_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `buyerId` int(11) DEFAULT NULL COMMENT '购买用户ID',
  `codeId` int(11) DEFAULT NULL COMMENT '激活码ID',
  `name` varchar(255) DEFAULT NULL COMMENT '学习包名称',
  `type` int(1) DEFAULT NULL COMMENT '类型 1题库 2视频 3题库+视频',
  `buy_mode` int(1) DEFAULT NULL COMMENT '购买方式 1支付购买 2激活码',
  `price` decimal(8,2) DEFAULT NULL COMMENT '价格',
  `introduce` varchar(255) DEFAULT NULL COMMENT '介绍',
  `begin_date` date DEFAULT NULL COMMENT '有效期始',
  `end_date` date DEFAULT NULL COMMENT '有效期止',
  `actual_price` decimal(8,2) DEFAULT NULL COMMENT '实际价格',
  `couponId` int(11) DEFAULT NULL COMMENT '优惠券ID',
  `discount` decimal(8,2) DEFAULT '0.00' COMMENT '折扣价格',
  `videoCourseId` int(11) DEFAULT NULL COMMENT '视频课程ID',
  `pay_mode` int(2) DEFAULT NULL COMMENT '支付方式 1支付宝 2微信 3支付宝电脑支付 4微信扫码 5IOS内购',
  `order_number` varchar(50) DEFAULT NULL COMMENT '订单号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `jh` varchar(50) DEFAULT NULL,
  `packageId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `courseInfoId` (`courseInfoId`),
  KEY `buyerId` (`buyerId`)
) ENGINE=InnoDB AUTO_INCREMENT=11772 DEFAULT CHARSET=utf8 COMMENT='用户学习包购买日志表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_coupon 结构
DROP TABLE IF EXISTS `t_coupon`;
CREATE TABLE IF NOT EXISTS `t_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type` int(2) DEFAULT NULL COMMENT '优惠券类型',
  `price` decimal(8,2) DEFAULT NULL COMMENT '抵扣价格',
  `period` int(6) DEFAULT NULL COMMENT '有效时间，月',
  `number` int(6) DEFAULT NULL COMMENT '优惠券数量',
  `time_limit` int(6) DEFAULT NULL COMMENT '分享优惠券时限',
  `used_number` int(6) DEFAULT NULL COMMENT '使用数量',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='优惠券信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_course_classify 结构
DROP TABLE IF EXISTS `t_course_classify`;
CREATE TABLE IF NOT EXISTS `t_course_classify` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) NOT NULL DEFAULT '0' COMMENT '分类名称',
  `orders` int(5) NOT NULL COMMENT '顺序',
  `pic` varchar(120) NOT NULL DEFAULT '0' COMMENT '分类图片',
  `isStop` varchar(2) NOT NULL DEFAULT '0' COMMENT '是否停用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COMMENT='课程分类信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_course_classify_sub 结构
DROP TABLE IF EXISTS `t_course_classify_sub`;
CREATE TABLE IF NOT EXISTS `t_course_classify_sub` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) NOT NULL COMMENT '课程分类ID',
  `name` varchar(255) DEFAULT NULL COMMENT '分类名称',
  `orders` int(5) DEFAULT NULL COMMENT '顺序',
  `isStop` varchar(2) DEFAULT NULL COMMENT '是否停用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COMMENT='课程分类小类信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_course_info 结构
DROP TABLE IF EXISTS `t_course_info`;
CREATE TABLE IF NOT EXISTS `t_course_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程大类ID',
  `classifyName` varchar(255) DEFAULT NULL COMMENT '课程大类名称',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `classifySubName` varchar(255) DEFAULT NULL COMMENT '课程小类名称',
  `name` varchar(255) DEFAULT NULL COMMENT '课程名称',
  `buy_url` varchar(120) DEFAULT NULL COMMENT '购买网址',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `real_price` decimal(10,2) DEFAULT NULL COMMENT '实际售价',
  `trade_price` decimal(10,2) DEFAULT NULL COMMENT '代理批发价',
  `retail_price` decimal(10,2) DEFAULT NULL COMMENT '建议零售价',
  `period` int(6) DEFAULT NULL COMMENT '有效日期，月',
  `orders` int(5) DEFAULT NULL COMMENT '显示顺序',
  `isStop` varchar(2) DEFAULT NULL COMMENT '是否停用',
  `allow_try` varchar(2) DEFAULT NULL COMMENT '允许试用',
  `dist_area` varchar(2) DEFAULT NULL COMMENT '分地区',
  `pic` varchar(120) DEFAULT NULL COMMENT '显示图片',
  `try_count` int(11) DEFAULT '0' COMMENT '试用人数',
  `buy_count` int(11) DEFAULT '0' COMMENT '购买人数',
  `random_buy_count` int(11) DEFAULT '0' COMMENT '随机卷购买人数',
  `total_score` int(3) DEFAULT NULL COMMENT '试卷总分数',
  `total_time` int(3) DEFAULT NULL COMMENT '考试总时长（分）',
  `single_score` decimal(5,2) DEFAULT NULL COMMENT '单选题分数',
  `single_number` int(3) DEFAULT NULL COMMENT '单选题数量',
  `multi_score` decimal(5,2) DEFAULT NULL COMMENT '多选题分数',
  `multi_number` int(3) DEFAULT NULL COMMENT '多选题数量',
  `judge_score` decimal(5,2) DEFAULT NULL COMMENT '判断题分数',
  `judge_number` int(3) DEFAULT NULL COMMENT '判断题数量',
  `undefined_score` decimal(5,2) DEFAULT NULL COMMENT '不定项分数',
  `undefined_number` int(3) DEFAULT NULL COMMENT '不定项数量',
  `question_score` decimal(5,2) DEFAULT NULL COMMENT '问答题分数',
  `question_number` int(3) DEFAULT NULL COMMENT '问答题数量',
  `case_score` decimal(5,2) DEFAULT NULL COMMENT '案例题分数',
  `case_number` int(3) DEFAULT NULL COMMENT '案例题数量',
  `fill_score` decimal(5,2) DEFAULT NULL COMMENT '填空题分数',
  `fill_number` int(3) DEFAULT NULL COMMENT '填空题数量',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `classifyId` (`classifyId`),
  KEY `classifySubId` (`classifySubId`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8 COMMENT='课程基础信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_error_correction 结构
DROP TABLE IF EXISTS `t_error_correction`;
CREATE TABLE IF NOT EXISTS `t_error_correction` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `testId` int(11) DEFAULT NULL COMMENT '章节/试卷ID',
  `questionId` int(11) DEFAULT NULL COMMENT '题目ID',
  `presenterId` int(11) DEFAULT NULL COMMENT '提交人ID',
  `error_type` varchar(30) DEFAULT NULL COMMENT '错误类型',
  `error_content` varchar(255) DEFAULT NULL COMMENT '错误内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT '0' COMMENT '状态 0待审核 1已审核',
  PRIMARY KEY (`id`),
  KEY `courseInfoId` (`courseInfoId`),
  KEY `testId` (`testId`),
  KEY `questionId` (`questionId`),
  KEY `presenterId` (`presenterId`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='错题纠错信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_exam_article 结构
DROP TABLE IF EXISTS `t_exam_article`;
CREATE TABLE IF NOT EXISTS `t_exam_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `type` int(1) DEFAULT NULL COMMENT '类型 1考试资讯 2考试指南',
  `image` varchar(120) DEFAULT NULL COMMENT '文章相关图片',
  `title` varchar(255) DEFAULT NULL COMMENT '文章标题',
  `content` longtext COMMENT '文章内容',
  `source` varchar(50) DEFAULT NULL COMMENT '文章来源',
  `views` int(11) DEFAULT NULL COMMENT '浏览次数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `classifySubId` (`classifySubId`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COMMENT='考试文章信息表（考试资讯和考试指南）';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_exercise_final 结构
DROP TABLE IF EXISTS `t_exercise_final`;
CREATE TABLE IF NOT EXISTS `t_exercise_final` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `test_type` varchar(2) DEFAULT NULL COMMENT '试卷类型 对应t_setting表test_type',
  `testId` int(11) DEFAULT NULL COMMENT '试卷ID',
  `exerciseId` int(11) DEFAULT NULL COMMENT '练习日志ID',
  `examId` int(11) DEFAULT NULL COMMENT '随机试卷ID',
  `answerId` int(11) DEFAULT NULL COMMENT '答题人ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `testId` (`testId`),
  KEY `logId` (`exerciseId`),
  KEY `answerId` (`answerId`),
  KEY `examId` (`examId`)
) ENGINE=InnoDB AUTO_INCREMENT=4498 DEFAULT CHARSET=utf8 COMMENT='最近练习信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_exercise_log 结构
DROP TABLE IF EXISTS `t_exercise_log`;
CREATE TABLE IF NOT EXISTS `t_exercise_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `testId` int(11) DEFAULT NULL COMMENT '试卷ID',
  `test_type` varchar(2) DEFAULT NULL COMMENT '试卷类型 对应t_setting表test_type',
  `answerId` int(11) DEFAULT NULL COMMENT '答题人ID',
  `examId` int(11) DEFAULT NULL COMMENT '随机试卷ID',
  `name` varchar(255) DEFAULT NULL COMMENT '练习名称',
  `model` int(2) DEFAULT NULL COMMENT '答题模式 1顺序 2浏览 3组卷',
  `begin_time` datetime DEFAULT NULL COMMENT '开始答题时间',
  `end_time` datetime DEFAULT NULL COMMENT '答题截止时间',
  `question_number` int(6) DEFAULT NULL COMMENT '题目数量',
  `answer_number` int(6) DEFAULT NULL COMMENT '答题数量',
  `correct_number` int(6) DEFAULT NULL COMMENT '正确答题数',
  `current_question` int(11) DEFAULT NULL COMMENT '当前练习题ID',
  `current_question_order` int(5) DEFAULT NULL COMMENT '当前题目顺序',
  `total_score` int(5) DEFAULT NULL COMMENT '练习总分数',
  `total_time` int(3) DEFAULT NULL COMMENT '考试总时间，分钟',
  `score` decimal(5,2) DEFAULT '0.00' COMMENT '得分',
  `accuracy` decimal(5,2) DEFAULT NULL COMMENT '正确率',
  `finish_time` datetime DEFAULT NULL COMMENT '完成时间',
  `cost_time` int(11) DEFAULT NULL COMMENT '用时，秒',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `bankId` (`testId`),
  KEY `answerId` (`answerId`),
  KEY `courseInfoId` (`courseInfoId`)
) ENGINE=InnoDB AUTO_INCREMENT=3549 DEFAULT CHARSET=utf8 COMMENT='练习日志表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_exercise_question 结构
DROP TABLE IF EXISTS `t_exercise_question`;
CREATE TABLE IF NOT EXISTS `t_exercise_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `testId` int(11) DEFAULT NULL COMMENT '章节/时间ID',
  `test_type` varchar(2) DEFAULT NULL COMMENT '试卷类型 对应t_setting表test_type',
  `exerciseId` int(11) DEFAULT NULL COMMENT '练习ID',
  `questionId` int(11) DEFAULT NULL COMMENT '题目ID',
  `answerId` int(11) DEFAULT NULL COMMENT '答题人ID',
  `standard_answer` varchar(255) DEFAULT NULL COMMENT '标准答案',
  `my_answer` varchar(255) DEFAULT NULL COMMENT '我的答案',
  `is_correct` int(1) DEFAULT NULL COMMENT '是否正确 0否 1是',
  `question_score` double(5,2) DEFAULT NULL COMMENT '题目分数',
  `score` double(5,2) DEFAULT NULL COMMENT '得分',
  `answer_time` datetime DEFAULT NULL COMMENT '答题时间',
  `time` int(6) DEFAULT NULL COMMENT '用时',
  `orders` int(5) DEFAULT NULL COMMENT '答题顺序',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionId_2` (`questionId`,`exerciseId`),
  KEY `exerciseId` (`exerciseId`),
  KEY `questionId` (`questionId`),
  KEY `courseInfoId` (`courseInfoId`),
  KEY `answerId` (`answerId`)
) ENGINE=InnoDB AUTO_INCREMENT=303771 DEFAULT CHARSET=utf8 COMMENT='练习题目信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_goods 结构
DROP TABLE IF EXISTS `t_goods`;
CREATE TABLE IF NOT EXISTS `t_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `price` decimal(8,2) DEFAULT NULL COMMENT '价格',
  `coin` decimal(8,2) DEFAULT NULL COMMENT '金币数',
  `discount` decimal(3,1) DEFAULT NULL COMMENT '折扣',
  `ios_id` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT 'ios商品ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `status` int(1) DEFAULT NULL COMMENT '状态 0禁用 1正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='虚拟商品价格表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_indexnews 结构
DROP TABLE IF EXISTS `t_indexnews`;
CREATE TABLE IF NOT EXISTS `t_indexnews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `context` varchar(255) DEFAULT '主页推送内容',
  `imgurl` varchar(255) DEFAULT NULL COMMENT '主页推送图片',
  `url` varchar(255) DEFAULT NULL COMMENT '跳转地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 examdb.t_learning_package 结构
DROP TABLE IF EXISTS `t_learning_package`;
CREATE TABLE IF NOT EXISTS `t_learning_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `name` varchar(50) DEFAULT NULL COMMENT '学习包名称',
  `price` decimal(8,2) DEFAULT NULL COMMENT '学习包价格',
  `introduce` varchar(255) DEFAULT NULL COMMENT '学习包介绍',
  `type` int(1) DEFAULT NULL COMMENT '学习包类型 1题库 2视频 3题库+视频',
  `period` int(6) DEFAULT NULL COMMENT '有效日期，月',
  `videoCourseId` int(11) DEFAULT NULL COMMENT '视频课程ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `courseInfoId` (`courseInfoId`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='学习包信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_lecturer 结构
DROP TABLE IF EXISTS `t_lecturer`;
CREATE TABLE IF NOT EXISTS `t_lecturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(30) DEFAULT NULL COMMENT '姓名',
  `head_pic` varchar(120) DEFAULT NULL COMMENT '头像',
  `levels` varchar(30) DEFAULT NULL COMMENT '级别  t_setting表lecturer_levels',
  `introduce` varchar(1000) DEFAULT NULL COMMENT '讲师介绍',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='课程讲师信息管理';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_menuinfo 结构
DROP TABLE IF EXISTS `t_menuinfo`;
CREATE TABLE IF NOT EXISTS `t_menuinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `menuurl` varchar(50) DEFAULT NULL COMMENT '菜单链接',
  `fatherid` int(11) DEFAULT NULL COMMENT '父节点ID',
  `orders` int(5) DEFAULT NULL COMMENT '显示顺序',
  `children` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111418 DEFAULT CHARSET=utf8 COMMENT='系统菜单功能表';
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (1, '题库管理', '', 0, 2, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (2, '视频管理', '', 0, 3, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (5, '运营管理', '', 0, 5, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (8, '错题管理', 'errorcorrection/index', 1, 3, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (9, '备考专区', 'examarticle/index', 111410, 8, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (10, '视频源管理', 'videoinfo/index', 2, 2, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (11, '销售汇总表', 'showUserBuyLog.json', 5, 7, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (12, '视频观看分析', 'showVideoOpenLog.json', 2, 4, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (13, '用户统计-按课程', 'showUserCounts.json', 5, 3, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (14, '用户信息管理', 'user/index', 5, 2, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (15, '学习包管理', 'learningpackage/index', 111414, 1, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (17, '订单管理', 'learningpackage/buy_index', 5, 1, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (18, '优惠券管理', 'coupon/index', 111414, 4, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (19, '公告管理', 'banner/index', 111415, 3, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (20, '支付接口管理', 'pay/interface', 5, 4, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (21, '未支付订单管理', 'order/index', 5, 5, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (22, '角色管理', 'showRoles.json', 111415, 6, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (23, '管理员账户', 'showAdmin.json', 111415, 5, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (24, '管理员操作记录', 'showLog.json', 111415, 7, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (25, '课程分类', 'course/index', 111410, 1, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (27, '课程信息', 'courseinfo/index', 111410, 2, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (28, '课程章节定义', 'testinfo/chapter_index', 111410, 3, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (29, '课程试卷管理', 'testinfo/test_index', 111410, 5, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (30, '备份库管理', 'questionBak/index', 1, 2, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (31, '使用库管理', 'question/index', 1, 1, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (32, '考点精华', 'testEssence/index', 111410, 7, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (35, '消息管理', 'message/index', 111415, 1, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (44, '讲师管理', 'lecturer/index', 2, 3, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (55, '系统信息', 'systeminfo/index', 111415, 2, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (111330, '视频课程信息', 'videocourse/index', 2, 1, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (111332, '菜单栏管理', 'showMenu.json', 111415, 8, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (111396, '激活码管理', 'activecode/index', 111414, 2, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (111397, '考前密押时间管理', 'opentime/index', 111410, 9, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (111398, '销售明细表', 'showUserBuyLogM.json', 5, 8, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (111407, '版本更新管理', 'systeminfo/app_version', 111415, 4, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (111408, '虚拟商品管理', 'goods/index', 111414, 3, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (111409, '充值记录管理', 'recharge/index', 5, 6, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (111410, '课程管理', '', 0, 1, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (111411, '题型管理', 'questionType/index', 111410, 6, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (111413, '预测演练', 'testinfo/chapter_index?1', 111410, 4, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (111414, '费用管理', '', 0, 4, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (111415, '系统管理', '', 0, 6, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (111416, '销售明细表（视频）', 'showUserBuyLogM_SP.json', 5, 9, NULL);
INSERT INTO `t_menuinfo` (`id`, `name`, `menuurl`, `fatherid`, `orders`, `children`) VALUES (111417, '用户学习情况统计', 'showUserLearn.json', 5, 10, NULL);

-- 数据导出被取消选择。
-- 导出  表 examdb.t_message 结构
DROP TABLE IF EXISTS `t_message`;
CREATE TABLE IF NOT EXISTS `t_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` varchar(255) DEFAULT NULL COMMENT '消息标题',
  `introduce` varchar(1000) DEFAULT NULL COMMENT '消息简介',
  `content` longtext COMMENT '消息内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_open_time 结构
DROP TABLE IF EXISTS `t_open_time`;
CREATE TABLE IF NOT EXISTS `t_open_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `test_type` varchar(2) COLLATE utf8_bin DEFAULT NULL COMMENT '试卷/章节类型  对应t_setting表test_type',
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '名称',
  `begin_date` date DEFAULT NULL COMMENT '开始日期',
  `end_date` date DEFAULT NULL COMMENT '结束日期',
  `overdue_tip` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '迟到的提示',
  `early_tip` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '过早的提示',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `courseInfoId` (`courseInfoId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='考前密押开启时间管理表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_order 结构
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE IF NOT EXISTS `t_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `packageId` int(11) DEFAULT NULL COMMENT '学习包ID',
  `buyerId` int(11) DEFAULT NULL COMMENT '购买用户ID',
  `price` decimal(8,2) DEFAULT NULL COMMENT '价格',
  `couponId` int(11) DEFAULT NULL COMMENT '优惠券ID',
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '订单名称',
  `type` int(1) DEFAULT NULL COMMENT '类型 1题库 2视频 3题库+视频',
  `introduce` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '介绍',
  `pay_mode` int(2) DEFAULT NULL COMMENT '支付方式 1支付宝 2微信 3支付宝电脑支付 4微信扫码',
  `videoCourseId` int(11) DEFAULT NULL COMMENT '视频课程ID',
  `discount` decimal(8,2) DEFAULT '0.00' COMMENT '折扣价格',
  `actual_price` decimal(8,2) DEFAULT NULL COMMENT '实际价格',
  `order_number` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '订单号',
  `begin_date` date DEFAULT NULL COMMENT '有效期始',
  `end_date` date DEFAULT NULL COMMENT '有效期止',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_number` (`order_number`) USING BTREE,
  KEY `courseInfoId` (`courseInfoId`),
  KEY `packageId` (`packageId`),
  KEY `buyerId` (`buyerId`)
) ENGINE=InnoDB AUTO_INCREMENT=278 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='订单信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_power 结构
DROP TABLE IF EXISTS `t_power`;
CREATE TABLE IF NOT EXISTS `t_power` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menuid` int(11) DEFAULT NULL,
  `roleid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4378 DEFAULT CHARSET=utf8 COMMENT='系统角色权限表';
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4334, 1, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4335, 8, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4336, 30, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4337, 31, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4338, 2, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4339, 10, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4340, 12, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4341, 44, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4342, 111330, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4343, 5, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4344, 11, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4345, 13, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4346, 14, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4347, 17, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4348, 20, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4349, 21, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4350, 111398, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4351, 111409, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4352, 111416, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4353, 111417, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4354, 111410, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4355, 9, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4356, 25, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4357, 27, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4358, 28, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4359, 29, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4360, 32, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4361, 111397, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4362, 111411, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4363, 111413, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4364, 111414, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4365, 15, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4366, 18, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4367, 111396, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4368, 111408, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4369, 111415, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4370, 19, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4371, 22, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4372, 23, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4373, 24, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4374, 35, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4375, 55, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4376, 111332, 1);
INSERT INTO `t_power` (`id`, `menuid`, `roleid`) VALUES (4377, 111407, 1);

-- 数据导出被取消选择。
-- 导出  表 examdb.t_question 结构
DROP TABLE IF EXISTS `t_question`;
CREATE TABLE IF NOT EXISTS `t_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程信息ID',
  `testId` int(11) DEFAULT NULL COMMENT '章节/试卷ID',
  `title` text COMMENT '题目标题',
  `content` text COMMENT '题目内容',
  `type` varchar(30) DEFAULT NULL COMMENT '题目类型',
  `score` decimal(5,2) DEFAULT NULL COMMENT '分数',
  `answer_number` int(2) DEFAULT NULL COMMENT '答案数量',
  `answer` text COMMENT '答案',
  `answer_analysis` text COMMENT '答案解析',
  `orders` int(5) DEFAULT NULL COMMENT '题目顺序',
  `isStop` varchar(2) DEFAULT NULL COMMENT '是否停用',
  `type_name` varchar(30) DEFAULT NULL COMMENT '题目类型名称',
  `answer_times` int(11) DEFAULT '0' COMMENT '作答次数',
  `correct_times` int(11) DEFAULT '0' COMMENT '正确作答次数',
  `accuracy` decimal(5,2) DEFAULT NULL COMMENT '正确率',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `classifyId` (`classifyId`),
  KEY `classifySubId` (`classifySubId`),
  KEY `classifyInfoId` (`courseInfoId`),
  KEY `testId` (`testId`)
) ENGINE=InnoDB AUTO_INCREMENT=533332 DEFAULT CHARSET=utf8 COMMENT='考试题目信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_question_bak 结构
DROP TABLE IF EXISTS `t_question_bak`;
CREATE TABLE IF NOT EXISTS `t_question_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程信息ID',
  `testBakId` int(11) DEFAULT NULL COMMENT '章节/试卷ID(备份库表)',
  `title` varchar(4000) DEFAULT NULL COMMENT '题目标题',
  `content` varchar(4000) DEFAULT NULL COMMENT '题目内容',
  `type` varchar(30) DEFAULT NULL COMMENT '题目类型',
  `score` decimal(5,2) DEFAULT NULL COMMENT '分数',
  `answer_number` int(2) DEFAULT NULL COMMENT '答案数量',
  `answer` varchar(255) DEFAULT NULL COMMENT '答案',
  `answer_analysis` varchar(4000) DEFAULT NULL COMMENT '答案解析',
  `orders` int(5) DEFAULT NULL COMMENT '题目顺序',
  `type_name` varchar(30) DEFAULT NULL COMMENT '题目类型名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `classifyId` (`classifyId`),
  KEY `classifySubId` (`classifySubId`),
  KEY `courseInfoId` (`courseInfoId`),
  KEY `testId` (`testBakId`)
) ENGINE=InnoDB AUTO_INCREMENT=19665 DEFAULT CHARSET=utf8 COMMENT='考试题目信息备份库表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_question_bak_20181014 结构
DROP TABLE IF EXISTS `t_question_bak_20181014`;
CREATE TABLE IF NOT EXISTS `t_question_bak_20181014` (
  `id` int(11) NOT NULL DEFAULT '0' COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程信息ID',
  `testId` int(11) DEFAULT NULL COMMENT '章节/试卷ID',
  `title` varchar(8000) CHARACTER SET utf8 DEFAULT NULL COMMENT '题目标题',
  `content` varchar(6000) CHARACTER SET utf8 DEFAULT NULL COMMENT '题目内容',
  `type` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '题目类型',
  `score` decimal(5,2) DEFAULT NULL COMMENT '分数',
  `answer_number` int(2) DEFAULT NULL COMMENT '答案数量',
  `answer` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '答案',
  `answer_analysis` varchar(7000) CHARACTER SET utf8 DEFAULT NULL COMMENT '答案解析',
  `orders` int(5) DEFAULT NULL COMMENT '题目顺序',
  `isStop` varchar(2) CHARACTER SET utf8 DEFAULT NULL COMMENT '是否停用',
  `type_name` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '题目类型名称',
  `answer_times` int(11) DEFAULT '0' COMMENT '作答次数',
  `correct_times` int(11) DEFAULT '0' COMMENT '正确作答次数',
  `accuracy` decimal(5,2) DEFAULT NULL COMMENT '正确率',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 数据导出被取消选择。
-- 导出  表 examdb.t_question_collect 结构
DROP TABLE IF EXISTS `t_question_collect`;
CREATE TABLE IF NOT EXISTS `t_question_collect` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `testId` int(11) DEFAULT NULL COMMENT '试卷ID',
  `test_type` varchar(2) DEFAULT NULL COMMENT '试卷类型 对应t_setting表test_type',
  `questionId` int(11) DEFAULT NULL COMMENT '题目ID',
  `collectorId` int(11) DEFAULT NULL COMMENT '收藏者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `courseInfoId` (`courseInfoId`),
  KEY `testId` (`testId`),
  KEY `questionId` (`questionId`),
  KEY `collectorId` (`collectorId`)
) ENGINE=InnoDB AUTO_INCREMENT=23805 DEFAULT CHARSET=utf8 COMMENT='题目收藏信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_question_error 结构
DROP TABLE IF EXISTS `t_question_error`;
CREATE TABLE IF NOT EXISTS `t_question_error` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `testId` int(11) DEFAULT NULL COMMENT '试卷ID',
  `test_type` varchar(2) DEFAULT NULL COMMENT '试卷类型 对应t_setting表test_type',
  `questionId` int(11) DEFAULT NULL COMMENT '题目ID',
  `answerId` int(11) DEFAULT NULL COMMENT '答题人ID',
  `isSolve` int(1) DEFAULT NULL COMMENT '是否解决 0否 1是',
  `error_times` int(6) DEFAULT NULL COMMENT '错误次数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionId_2` (`questionId`,`answerId`),
  KEY `questionId` (`questionId`),
  KEY `answerId` (`answerId`),
  KEY `courseInfoId` (`courseInfoId`),
  KEY `testId` (`testId`)
) ENGINE=InnoDB AUTO_INCREMENT=164922 DEFAULT CHARSET=utf8 COMMENT='错题信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_question_ids 结构
DROP TABLE IF EXISTS `t_question_ids`;
CREATE TABLE IF NOT EXISTS `t_question_ids` (
  `id` int(11) NOT NULL DEFAULT '0' COMMENT '主键'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 数据导出被取消选择。
-- 导出  表 examdb.t_question_note 结构
DROP TABLE IF EXISTS `t_question_note`;
CREATE TABLE IF NOT EXISTS `t_question_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `testId` int(11) DEFAULT NULL COMMENT '试卷ID',
  `test_type` varchar(2) DEFAULT NULL COMMENT '试卷类型 对应t_setting表test_type',
  `questionId` int(11) DEFAULT NULL COMMENT '题目ID',
  `ownerId` int(11) DEFAULT NULL COMMENT '所有者ID',
  `content` varchar(4000) DEFAULT NULL COMMENT '笔记内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `courseInfoId` (`courseInfoId`),
  KEY `testId` (`testId`),
  KEY `questionId` (`questionId`),
  KEY `ownerId` (`ownerId`)
) ENGINE=InnoDB AUTO_INCREMENT=1847 DEFAULT CHARSET=utf8 COMMENT='用户笔记信息';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_question_type 结构
DROP TABLE IF EXISTS `t_question_type`;
CREATE TABLE IF NOT EXISTS `t_question_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '默认名称',
  `custom_name` varchar(50) DEFAULT NULL COMMENT '自定义名称',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COMMENT='题型名称表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_random_config 结构
DROP TABLE IF EXISTS `t_random_config`;
CREATE TABLE IF NOT EXISTS `t_random_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parentId` int(11) DEFAULT NULL COMMENT '章节ID',
  `testId` int(11) DEFAULT NULL COMMENT '小节ID',
  `number` int(11) DEFAULT NULL COMMENT '题目数量',
  `score` decimal(5,2) DEFAULT NULL COMMENT '题目分数',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `testId` (`testId`),
  KEY `parentId` (`parentId`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='随机组卷参数设置信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_random_exam 结构
DROP TABLE IF EXISTS `t_random_exam`;
CREATE TABLE IF NOT EXISTS `t_random_exam` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `testId` int(11) DEFAULT NULL COMMENT '章节ID',
  `answerId` int(11) DEFAULT NULL COMMENT '用户ID',
  `name` varchar(255) DEFAULT NULL COMMENT '试卷名称',
  `question_number` int(6) DEFAULT NULL COMMENT '题目数量',
  `total_score` int(5) DEFAULT NULL COMMENT '试卷总分数',
  `total_time` int(3) DEFAULT NULL COMMENT '考试时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `courseInfoId` (`courseInfoId`),
  KEY `answerId` (`answerId`)
) ENGINE=InnoDB AUTO_INCREMENT=673 DEFAULT CHARSET=utf8 COMMENT='模拟考试随机试卷信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_random_question 结构
DROP TABLE IF EXISTS `t_random_question`;
CREATE TABLE IF NOT EXISTS `t_random_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `testId` int(11) DEFAULT NULL COMMENT '章节/时间ID',
  `test_type` varchar(2) DEFAULT NULL COMMENT '试卷类型 对应t_setting表test_type',
  `examId` int(11) DEFAULT NULL COMMENT '练习ID',
  `questionId` int(11) DEFAULT NULL COMMENT '题目ID',
  `answerId` int(11) DEFAULT NULL COMMENT '答题人ID',
  `standard_answer` varchar(255) DEFAULT NULL COMMENT '标准答案',
  `question_score` decimal(5,2) DEFAULT NULL COMMENT '题目分数',
  `orders` int(11) DEFAULT NULL COMMENT '答题顺序',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `courseInfoId` (`courseInfoId`),
  KEY `testId` (`testId`),
  KEY `questionId` (`questionId`),
  KEY `answerId` (`answerId`),
  KEY `examId` (`examId`)
) ENGINE=InnoDB AUTO_INCREMENT=89972 DEFAULT CHARSET=utf8 COMMENT='模拟考试随机试题信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_recharge_log 结构
DROP TABLE IF EXISTS `t_recharge_log`;
CREATE TABLE IF NOT EXISTS `t_recharge_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userId` int(11) DEFAULT NULL COMMENT '用户ID',
  `amount` decimal(8,2) DEFAULT NULL COMMENT '金额',
  `coin` decimal(8,2) DEFAULT NULL,
  `pay_mode` int(2) DEFAULT NULL COMMENT '支付方式',
  `order_number` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '订单编号',
  `ios_id` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT 'ios商品ID',
  `transaction_id` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT 'ios订单编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `status` int(1) DEFAULT NULL COMMENT '状态 0初始化 1成功',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `order_number` (`order_number`),
  KEY `create_time` (`create_time`)
) ENGINE=InnoDB AUTO_INCREMENT=639 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户充值日志表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_role 结构
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE IF NOT EXISTS `t_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COMMENT='系统角色信息表';

INSERT INTO `t_role` (`id`, `name`) VALUES (1, '管理员');

-- 数据导出被取消选择。
-- 导出  表 examdb.t_setting 结构
DROP TABLE IF EXISTS `t_setting`;
CREATE TABLE IF NOT EXISTS `t_setting` (
  `id` int(22) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type` varchar(50) DEFAULT NULL COMMENT '类型',
  `code` varchar(255) DEFAULT NULL COMMENT '代码',
  `value` varchar(4000) DEFAULT NULL COMMENT '值',
  `value2` varchar(255) DEFAULT NULL COMMENT '值2',
  `value3` varchar(255) DEFAULT NULL COMMENT '值3',
  `description` varchar(255) DEFAULT NULL COMMENT '说明',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(22) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(22) DEFAULT NULL COMMENT '修改人',
  `status` int(11) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COMMENT='系统配置表';
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (16, 'sys', 'BASE_PATH', 'C:\\', NULL, NULL, '文件存储根路径', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (17, 'sys', 'COURSE_CLASSIFY_PATH', 'upload\\course\\classify\\', NULL, NULL, '课程分类图片路径', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (18, 'question_type', '1', '单选题', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (19, 'question_type', '2', '多选题', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (20, 'question_type', '3', '判断题', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (21, 'question_type', '4', '问答题', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (22, 'question_type', '5', '案例题', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (23, 'question_type', '6', '填空题', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (24, 'question_type', '7', '不定项选择题', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (25, 'sys', 'COURSE_INFO_PATH', 'upload\\course\\info\\', NULL, NULL, '课程档案信息图片路径', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (26, 'sys', 'QUESTION_PIC_PATH', 'upload\\question\\pic\\', NULL, NULL, '题目相关图片路径', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (27, 'sys', 'BANNER_IMAGE_PATH', 'upload\\banner\\image\\', NULL, NULL, '广告信息相关图片路径', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (28, 'sys', 'TEST_EASSENCE_IMAGE_PATH', 'upload\\testeassence\\image\\', NULL, NULL, '考点精华相关图片路径', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (29, 'test_type', '01', '章节练习', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (30, 'test_type', '02', '预测演练', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (31, 'test_type', '03', '历年真题', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (32, 'test_type', '04', '模拟考场', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (33, 'test_type', '05', '考前密押', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (34, 'lecturer_levels', '01', '初级讲师', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (35, 'lecturer_levels', '02', '中级讲师', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (36, 'lecturer_levels', '03', '高级讲师', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (37, 'sys', 'LECTURER_PIC_PATH', 'upload\\lecturer\\pic\\', NULL, NULL, '讲师相关图片路径', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (38, 'sys', 'USER_HEADPIC_PATH', 'upload\\user\\headpic\\', NULL, NULL, '用户头像图片路径', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (39, 'sys', 'ADMIN_IMAGE_PATH', 'upload\\admin\\image\\', NULL, NULL, '管理员头像路径', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (40, 'sys', 'ARTICLE_IMAGE_PATH', 'upload\\article\\image\\', NULL, NULL, '考试文章相关图片路径', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (41, 'sys', 'SYSTEM_INFO_IMAGE_PATH', 'upload\\system\\image\\', NULL, NULL, '系统信息相关图片', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (42, 'sys', 'MESSAGE_IMAGE_PATH', 'upload\\message\\', NULL, NULL, '消息相关图片', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (43, 'question_error_type', '01', '答案有问题', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (44, 'question_error_type', '02', '答案与解析不相符', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (45, 'question_error_type', '03', '有错别字', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (46, 'question_error_type', '04', '选项有问题', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (47, 'question_error_type', '05', '其他', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (48, 'sys', 'VIDEO_IMAGE_PATH', 'upload\\video\\image\\', NULL, NULL, '视频相关图片', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (49, 'micromsg', 'UNIFIED_ORDER_URL', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (50, 'micromsg', 'APPID', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (51, 'alipay', 'NOTIFY_URL', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (52, 'micromsg', 'MCH_ID', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (53, 'micromsg', 'API_KEY', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (54, 'micromsg', 'SIGN_TYPE', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (55, 'micromsg', 'TRADE_TYPE', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (56, 'micromsg', 'FEE_TYPE', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (57, 'micromsg', 'NOTIFY_URL', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (58, 'alipay', 'APP_ID', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (59, 'alipay', 'APP_PRIVATE_KEY', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (60, 'alipay', 'ALIPAY_PUBLIC_KEY', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (61, 'alipay', 'CHARSET', 'utf-8', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (62, 'alipay', 'SERVER_URL', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (63, 'alipay', 'FORMAT', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (64, 'alipay', 'SIGN_TYPE', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (65, 'micromsg', 'TRADE_TYPE_NATIVE', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (66, 'micromsg', 'APP_SECRET', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (67, 'ios_buy', 'BUNDLE_ID', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (68, 'upload_data', 'getVersionUrl', 'http://127.0.0.1/ExamCenter/upload/getVersion', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (69, 'upload_data', 'uploadDataUrl', 'http://127.0.0.1/ExamCenter/upload/uploadData', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (70, 'sys', 'IS_ONLINE', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (71, 'micromsg_mini', 'APP_ID', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (72, 'micromsg_mini', 'APP_SECRET', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (73, 'micromsg_mini', 'MCH_ID', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (74, 'micromsg_mini', 'API_KEY', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `t_setting` (`id`, `type`, `code`, `value`, `value2`, `value3`, `description`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (75, 'micromsg_mini', 'NOTIFY_URL', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- 数据导出被取消选择。
-- 导出  表 examdb.t_system_info 结构
DROP TABLE IF EXISTS `t_system_info`;
CREATE TABLE IF NOT EXISTS `t_system_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `image` varchar(120) DEFAULT NULL COMMENT '相关图片',
  `about` varchar(1000) DEFAULT NULL COMMENT '关于',
  `version` varchar(20) DEFAULT NULL COMMENT '版本信息',
  `website` varchar(50) DEFAULT NULL COMMENT '官方网站',
  `copyright` varchar(100) DEFAULT NULL COMMENT '版权所有',
  `law` text COMMENT '法律条款',
  `pay_agreement` text COMMENT '付费协议',
  `sms_signature` varchar(50) DEFAULT NULL COMMENT '短信签名',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统信息表';
INSERT INTO `t_system_info` (`id`, `image`, `about`, `version`, `website`, `copyright`, `law`, `pay_agreement`, `sms_signature`, `create_time`, `create_user_id`, `modify_time`, `modify_user_id`, `status`) VALUES (2, 'upload\\system\\image\\1f29b093-df00-4738-a9e5-6ea1a2d10ab0.jpg', '职考大师', '2017V.10', '', '', '                                    ', '职考大师', '2017-07-25 13:54:55', NULL, '2018-03-26 16:47:39', NULL, NULL);

-- 数据导出被取消选择。
-- 导出  表 examdb.t_test_essence 结构
DROP TABLE IF EXISTS `t_test_essence`;
CREATE TABLE IF NOT EXISTS `t_test_essence` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程大类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类DI',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程信息ID',
  `title` varchar(255) DEFAULT NULL COMMENT '精华考点标题',
  `content` longtext COMMENT '精华考点内容',
  `isStop` varchar(2) DEFAULT NULL COMMENT '是否停用',
  `orders` int(6) DEFAULT NULL COMMENT '显示顺序',
  `allow_try` varchar(2) DEFAULT NULL COMMENT '是否允许试用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='考点精华信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_test_info 结构
DROP TABLE IF EXISTS `t_test_info`;
CREATE TABLE IF NOT EXISTS `t_test_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程信息ID',
  `code` varchar(100) DEFAULT NULL COMMENT '编码',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `question_number` int(6) DEFAULT NULL COMMENT '试题数量',
  `area` varchar(20) DEFAULT NULL COMMENT '所属区域',
  `test_time` int(3) DEFAULT NULL COMMENT '考试时间',
  `test_score` int(3) DEFAULT NULL COMMENT '考试分数',
  `test_type` varchar(2) DEFAULT NULL COMMENT '试卷小类 对应t_setting表test_type',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `isStop` varchar(2) DEFAULT NULL COMMENT '是否停用',
  `allow_try` varchar(2) DEFAULT NULL COMMENT '是否允许试用',
  `levels` int(3) DEFAULT NULL COMMENT '等级',
  `parentId` int(11) DEFAULT NULL COMMENT '父节点ID',
  `is_expand` int(1) DEFAULT '0' COMMENT '是否展开 0否 1是',
  `avg_score` decimal(5,2) DEFAULT '0.00' COMMENT '平均得分',
  `avg_accuracy` decimal(5,2) DEFAULT '0.00' COMMENT '平均正确率',
  `is_new` int(1) DEFAULT '0' COMMENT '是否最新 0否 1是',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `classifyId` (`classifyId`),
  KEY `classifySubId` (`classifySubId`),
  KEY `classifyInfoId` (`courseInfoId`),
  KEY `parentId` (`parentId`)
) ENGINE=InnoDB AUTO_INCREMENT=4537 DEFAULT CHARSET=utf8 COMMENT='试卷/章节信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_test_info_bak 结构
DROP TABLE IF EXISTS `t_test_info_bak`;
CREATE TABLE IF NOT EXISTS `t_test_info_bak` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程信息ID',
  `code` varchar(100) DEFAULT NULL COMMENT '编码',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `question_number` int(6) DEFAULT NULL COMMENT '试题数量',
  `test_type` varchar(2) DEFAULT NULL COMMENT '试卷小类 对应t_setting表test_type',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `classifyId` (`classifyId`),
  KEY `classifySubId` (`classifySubId`),
  KEY `courseInfoId` (`courseInfoId`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COMMENT='试卷/章节信息备份库表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_test_question 结构
DROP TABLE IF EXISTS `t_test_question`;
CREATE TABLE IF NOT EXISTS `t_test_question` (
  `id` int(11) NOT NULL COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程信息ID',
  `testId` int(11) DEFAULT NULL COMMENT '章节/试卷ID',
  `title` varchar(6000) DEFAULT NULL COMMENT '题目标题',
  `content` varchar(6000) DEFAULT NULL COMMENT '题目内容',
  `type` varchar(30) DEFAULT NULL COMMENT '题目类型',
  `score` decimal(5,2) DEFAULT NULL COMMENT '分数',
  `answer_number` int(2) DEFAULT NULL COMMENT '答案数量',
  `answer` varchar(255) DEFAULT NULL COMMENT '答案',
  `answer_analysis` varchar(6000) DEFAULT NULL COMMENT '答案解析',
  `orders` int(5) DEFAULT NULL COMMENT '题目顺序',
  `isStop` varchar(2) DEFAULT NULL COMMENT '是否停用',
  `type_name` varchar(30) DEFAULT NULL COMMENT '题目类型名称',
  `answer_times` int(11) DEFAULT '0' COMMENT '作答次数',
  `correct_times` int(11) DEFAULT '0' COMMENT '正确作答次数',
  `accuracy` decimal(5,2) DEFAULT NULL COMMENT '正确率',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `tmp1` varchar(500) DEFAULT NULL,
  `tmp2` varchar(500) DEFAULT NULL,
  `tmp3` varchar(500) DEFAULT NULL,
  `tmp4` varchar(500) DEFAULT NULL,
  KEY `classifyId` (`classifyId`),
  KEY `classifySubId` (`classifySubId`),
  KEY `testId` (`testId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='考试题目信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_tmp_course_classify 结构
DROP TABLE IF EXISTS `t_tmp_course_classify`;
CREATE TABLE IF NOT EXISTS `t_tmp_course_classify` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) NOT NULL DEFAULT '0' COMMENT '分类名称',
  `orders` int(5) NOT NULL COMMENT '顺序',
  `pic` varchar(120) NOT NULL DEFAULT '0' COMMENT '分类图片',
  `isStop` varchar(2) NOT NULL DEFAULT '0' COMMENT '是否停用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `tmpid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='课程分类信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_tmp_course_classify_sub 结构
DROP TABLE IF EXISTS `t_tmp_course_classify_sub`;
CREATE TABLE IF NOT EXISTS `t_tmp_course_classify_sub` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) NOT NULL COMMENT '课程分类ID',
  `name` varchar(255) DEFAULT NULL COMMENT '分类名称',
  `orders` int(5) DEFAULT NULL COMMENT '顺序',
  `isStop` varchar(2) DEFAULT NULL COMMENT '是否停用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `tmpid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='课程分类小类信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_tmp_course_info 结构
DROP TABLE IF EXISTS `t_tmp_course_info`;
CREATE TABLE IF NOT EXISTS `t_tmp_course_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程大类ID',
  `classifyName` varchar(255) DEFAULT NULL COMMENT '课程大类名称',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `classifySubName` varchar(255) DEFAULT NULL COMMENT '课程小类名称',
  `name` varchar(255) DEFAULT NULL COMMENT '课程名称',
  `buy_url` varchar(120) DEFAULT NULL COMMENT '购买网址',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `real_price` decimal(10,2) DEFAULT NULL COMMENT '实际售价',
  `trade_price` decimal(10,2) DEFAULT NULL COMMENT '代理批发价',
  `retail_price` decimal(10,2) DEFAULT NULL COMMENT '建议零售价',
  `orders` int(5) DEFAULT NULL COMMENT '显示顺序',
  `isStop` varchar(2) DEFAULT NULL COMMENT '是否停用',
  `allow_try` varchar(2) DEFAULT NULL COMMENT '允许试用',
  `dist_area` varchar(2) DEFAULT NULL COMMENT '分地区',
  `pic` varchar(120) DEFAULT NULL COMMENT '显示图片',
  `try_count` int(11) DEFAULT '0' COMMENT '试用人数',
  `buy_count` int(11) DEFAULT '0' COMMENT '购买人数',
  `random_buy_count` int(11) DEFAULT '0' COMMENT '随机卷购买人数',
  `total_score` int(3) DEFAULT NULL COMMENT '试卷总分数',
  `total_time` int(3) DEFAULT NULL COMMENT '考试总时长（分）',
  `single_score` decimal(5,2) DEFAULT NULL COMMENT '单选题分数',
  `single_number` int(3) DEFAULT NULL COMMENT '单选题数量',
  `multi_score` decimal(5,2) DEFAULT NULL COMMENT '多选题分数',
  `multi_number` int(3) DEFAULT NULL COMMENT '多选题数量',
  `judge_score` decimal(5,2) DEFAULT NULL COMMENT '判断题分数',
  `judge_number` int(3) DEFAULT NULL COMMENT '判断题数量',
  `undefined_score` decimal(5,2) DEFAULT NULL COMMENT '不定项分数',
  `undefined_number` int(3) DEFAULT NULL COMMENT '不定项数量',
  `question_score` decimal(5,2) DEFAULT NULL COMMENT '问答题分数',
  `question_number` int(3) DEFAULT NULL COMMENT '问答题数量',
  `case_score` decimal(5,2) DEFAULT NULL COMMENT '案例题分数',
  `case_number` int(3) DEFAULT NULL COMMENT '案例题数量',
  `fill_score` decimal(5,2) DEFAULT NULL COMMENT '填空题分数',
  `fill_number` int(3) DEFAULT NULL COMMENT '填空题数量',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT '1' COMMENT '状态',
  `tmpid` varchar(50) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `classifyId` (`classifyId`),
  KEY `classifySubId` (`classifySubId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='课程基础信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_tmp_question 结构
DROP TABLE IF EXISTS `t_tmp_question`;
CREATE TABLE IF NOT EXISTS `t_tmp_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程信息ID',
  `testId` int(11) DEFAULT NULL COMMENT '章节/试卷ID',
  `title` varchar(8000) DEFAULT NULL COMMENT '题目标题',
  `content` varchar(4000) DEFAULT NULL COMMENT '题目内容',
  `type` varchar(30) DEFAULT NULL COMMENT '题目类型',
  `score` decimal(5,2) DEFAULT NULL COMMENT '分数',
  `answer_number` int(2) DEFAULT NULL COMMENT '答案数量',
  `answer` varchar(255) DEFAULT NULL COMMENT '答案',
  `answer_analysis` varchar(4000) DEFAULT NULL COMMENT '答案解析',
  `orders` int(5) DEFAULT NULL COMMENT '题目顺序',
  `isStop` varchar(2) DEFAULT NULL COMMENT '是否停用',
  `type_name` varchar(30) DEFAULT NULL COMMENT '题目类型名称',
  `answer_times` int(11) DEFAULT '0' COMMENT '作答次数',
  `correct_times` int(11) DEFAULT '0' COMMENT '正确作答次数',
  `accuracy` decimal(5,2) DEFAULT NULL COMMENT '正确率',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `tmpid` varchar(50) DEFAULT NULL,
  `tmpname` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tmpid` (`tmpid`),
  KEY `classifyId` (`classifyId`),
  KEY `classifySubId` (`classifySubId`),
  KEY `classifyInfoId` (`courseInfoId`),
  KEY `testId` (`testId`)
) ENGINE=InnoDB AUTO_INCREMENT=25800 DEFAULT CHARSET=utf8 COMMENT='考试题目信息表 参考答案和解析后面有换行';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_tmp_test_info 结构
DROP TABLE IF EXISTS `t_tmp_test_info`;
CREATE TABLE IF NOT EXISTS `t_tmp_test_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程信息ID',
  `code` varchar(100) DEFAULT NULL COMMENT '编码',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `question_number` int(6) DEFAULT NULL COMMENT '试题数量',
  `area` varchar(20) DEFAULT NULL COMMENT '所属区域',
  `test_time` int(3) DEFAULT NULL COMMENT '考试时间',
  `test_type` varchar(2) DEFAULT NULL COMMENT '试卷小类 对应t_setting表test_type',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `isStop` varchar(2) DEFAULT NULL COMMENT '是否停用',
  `allow_try` varchar(2) DEFAULT NULL COMMENT '是否允许试用',
  `levels` int(3) DEFAULT NULL COMMENT '等级',
  `parentId` int(11) DEFAULT NULL COMMENT '父节点ID',
  `is_expand` int(1) DEFAULT '0' COMMENT '是否展开 0否 1是',
  `avg_score` decimal(5,2) DEFAULT '0.00' COMMENT '平均得分',
  `avg_accuracy` decimal(5,2) DEFAULT '0.00' COMMENT '平均正确率',
  `is_new` int(1) DEFAULT '0' COMMENT '是否最新 0否 1是',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `classifyId` (`classifyId`),
  KEY `classifySubId` (`classifySubId`),
  KEY `classifyInfoId` (`courseInfoId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='试卷/章节信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_user 结构
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `account` varchar(11) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '账号',
  `phone` varchar(11) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手机号码',
  `password` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '登录密码',
  `sex` varchar(2) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '性别',
  `head_pic` varchar(120) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '头像',
  `nick_name` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '昵称',
  `signature` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '个性签名',
  `email` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '电子邮箱',
  `inviterId` int(11) DEFAULT NULL COMMENT '邀请人ID',
  `invite_phone` varchar(11) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '邀请人手机号码',
  `city` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '报考城市',
  `vip` int(11) DEFAULT '0' COMMENT '是否会员 0否 1是',
  `login_times` int(11) DEFAULT NULL COMMENT '登录次数',
  `last_login_time` datetime DEFAULT NULL COMMENT '最近登录时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT '1' COMMENT '状态',
  `jh` int(11) DEFAULT NULL,
  `coin` decimal(8,2) NOT NULL DEFAULT '0.00' COMMENT '用户账户金币',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7061 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_user_coupon 结构
DROP TABLE IF EXISTS `t_user_coupon`;
CREATE TABLE IF NOT EXISTS `t_user_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `coupon_type` int(1) DEFAULT NULL COMMENT '优惠券类型',
  `coupon_id` int(11) DEFAULT NULL COMMENT '优惠券ID',
  `ownerId` int(11) DEFAULT NULL COMMENT '用户ID',
  `price` decimal(8,2) DEFAULT NULL COMMENT '抵扣价格',
  `isUsed` int(1) DEFAULT NULL COMMENT '是否使用',
  `begin_date` date DEFAULT NULL COMMENT '有效期始',
  `end_date` date DEFAULT NULL COMMENT '有效期止',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `ownerId` (`ownerId`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=utf8 COMMENT='用户优惠券信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_user_coupon_receive 结构
DROP TABLE IF EXISTS `t_user_coupon_receive`;
CREATE TABLE IF NOT EXISTS `t_user_coupon_receive` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `inviteId` int(11) DEFAULT NULL COMMENT '邀请ID',
  `inviterId` int(11) DEFAULT NULL COMMENT '邀请人ID',
  `phone` varchar(11) COLLATE utf8_bin DEFAULT NULL COMMENT '用户手机号码',
  `price` decimal(8,2) DEFAULT NULL COMMENT '抵扣价格',
  `begin_date` date DEFAULT NULL COMMENT '有效期始',
  `end_date` date DEFAULT NULL COMMENT '有效期止',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `phone` (`phone`),
  KEY `inviteId` (`inviteId`),
  KEY `inviterId` (`inviterId`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户优惠券领用表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_user_device 结构
DROP TABLE IF EXISTS `t_user_device`;
CREATE TABLE IF NOT EXISTS `t_user_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userId` int(11) DEFAULT NULL COMMENT '用户ID',
  `deviceId` varchar(50) DEFAULT NULL COMMENT '设备ID',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  KEY `deviceId` (`deviceId`)
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8 COMMENT='用户和设备绑定表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_user_invite 结构
DROP TABLE IF EXISTS `t_user_invite`;
CREATE TABLE IF NOT EXISTS `t_user_invite` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `inviterId` int(11) DEFAULT NULL COMMENT '邀请人ID',
  `price` decimal(8,2) DEFAULT NULL COMMENT '抵扣价格',
  `period` int(6) DEFAULT NULL COMMENT '有效期',
  `end_time` datetime DEFAULT NULL COMMENT '分享链接结束时间',
  `number` int(6) DEFAULT NULL COMMENT '优惠券数量',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `inviterId` (`inviterId`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户邀请信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_user_message 结构
DROP TABLE IF EXISTS `t_user_message`;
CREATE TABLE IF NOT EXISTS `t_user_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userId` int(11) DEFAULT NULL COMMENT '用户ID',
  `messageId` int(11) DEFAULT NULL COMMENT '消息ID',
  `isRead` int(1) DEFAULT '0' COMMENT '是否阅读 0否 1是',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=utf8 COMMENT='用户消息信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_user_oplog 结构
DROP TABLE IF EXISTS `t_user_oplog`;
CREATE TABLE IF NOT EXISTS `t_user_oplog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tel` varchar(50) DEFAULT NULL COMMENT '操作人',
  `optime` datetime NOT NULL COMMENT '操作时间',
  `modal` varchar(50) NOT NULL COMMENT '访问模块',
  `operresult` varchar(50) DEFAULT NULL COMMENT '操作详情',
  `userip` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4903 DEFAULT CHARSET=utf8 COMMENT='用户操作日志';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_video_collect 结构
DROP TABLE IF EXISTS `t_video_collect`;
CREATE TABLE IF NOT EXISTS `t_video_collect` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `videoCourseId` int(11) DEFAULT NULL COMMENT '视频课程ID',
  `collectorId` int(11) DEFAULT NULL COMMENT '收藏用户ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `courseInfoId` (`courseInfoId`),
  KEY `videoCourseId` (`videoCourseId`),
  KEY `collectorId` (`collectorId`),
  KEY `classifySubId` (`classifySubId`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COMMENT='视频收藏信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_video_course 结构
DROP TABLE IF EXISTS `t_video_course`;
CREATE TABLE IF NOT EXISTS `t_video_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程分类小类ID',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `name` varchar(50) DEFAULT NULL COMMENT '视频课程名称',
  `introduce` varchar(4000) DEFAULT NULL COMMENT '视频课程介绍',
  `lecturerId` int(11) DEFAULT NULL COMMENT '讲师ID',
  `image` varchar(120) DEFAULT NULL COMMENT '封面图片',
  `score` decimal(5,2) DEFAULT NULL COMMENT '评分',
  `learn_number` int(11) DEFAULT NULL COMMENT '学习人数',
  `orders` int(5) DEFAULT NULL COMMENT '显示顺序',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `taocanid` varchar(20) DEFAULT NULL COMMENT '超思维套餐ID',
  `price` decimal(8,2) DEFAULT NULL COMMENT '价格',
  `period` int(6) DEFAULT NULL COMMENT '有效日期，月',
  PRIMARY KEY (`id`),
  KEY `courseInfoId` (`courseInfoId`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='视频课程信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_video_flow_log 结构
DROP TABLE IF EXISTS `t_video_flow_log`;
CREATE TABLE IF NOT EXISTS `t_video_flow_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `videoCourseId` int(11) DEFAULT NULL COMMENT '视频课程ID',
  `videoInfoId` int(11) DEFAULT NULL COMMENT '视频ID',
  `duration` int(11) DEFAULT NULL COMMENT '视频时长',
  `viewerId` int(11) DEFAULT NULL COMMENT '观看者ID',
  `type` int(1) DEFAULT NULL COMMENT '观看类型 1试看 2购买观看',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `courseInfoId` (`courseInfoId`),
  KEY `videoCourseId` (`videoCourseId`),
  KEY `videoInfoId` (`videoInfoId`)
) ENGINE=InnoDB AUTO_INCREMENT=938 DEFAULT CHARSET=utf8 COMMENT='视频观看流水日志表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_video_info 结构
DROP TABLE IF EXISTS `t_video_info`;
CREATE TABLE IF NOT EXISTS `t_video_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程信息ID',
  `videoCourseId` int(11) DEFAULT NULL COMMENT '视频课程ID',
  `source` int(2) DEFAULT NULL COMMENT '视频来源 1超思维 2腾讯云 3千牛云',
  `title` varchar(255) DEFAULT NULL COMMENT '视频章节标题',
  `url` varchar(255) DEFAULT NULL COMMENT '视频地址',
  `image` varchar(120) DEFAULT NULL COMMENT '封面图片',
  `watch_times` int(11) DEFAULT NULL COMMENT '观看次数',
  `try_watch_times` int(11) DEFAULT NULL COMMENT '试看次数',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `duration` int(11) DEFAULT NULL COMMENT '视频时长',
  `try_duration` int(11) DEFAULT NULL COMMENT '试看时长',
  `isStop` varchar(2) DEFAULT NULL COMMENT '是否停用',
  `allow_try` varchar(2) DEFAULT NULL COMMENT '是否允许试看',
  `levels` int(3) DEFAULT NULL COMMENT '等级',
  `parentId` int(11) DEFAULT NULL COMMENT '父节点ID',
  `orders` int(6) DEFAULT NULL COMMENT '显示顺序',
  `is_expand` int(1) DEFAULT '0' COMMENT '是否展开 0否 1是',
  `handouts` longtext COMMENT '视频讲义',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  `jh` int(11) DEFAULT NULL,
  `classid` varchar(20) DEFAULT NULL COMMENT '超思维班级ID',
  `videoid` varchar(20) DEFAULT NULL COMMENT '超思维视频ID',
  PRIMARY KEY (`id`),
  KEY `classifyId` (`classifyId`),
  KEY `classifySubId` (`classifySubId`),
  KEY `courseInfoId` (`courseInfoId`)
) ENGINE=InnoDB AUTO_INCREMENT=825 DEFAULT CHARSET=utf8 COMMENT='视频信息表';

-- 数据导出被取消选择。
-- 导出  表 examdb.t_video_log 结构
DROP TABLE IF EXISTS `t_video_log`;
CREATE TABLE IF NOT EXISTS `t_video_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classifyId` int(11) DEFAULT NULL COMMENT '课程分类ID',
  `classifySubId` int(11) DEFAULT NULL COMMENT '课程小类ID',
  `courseInfoId` int(11) DEFAULT NULL COMMENT '课程ID',
  `videoCourseId` int(11) DEFAULT NULL COMMENT '视频课程ID',
  `videoInfoId` int(11) DEFAULT NULL COMMENT '视频信息ID',
  `duration` int(11) DEFAULT NULL COMMENT '视频时长',
  `play_duration` int(11) DEFAULT NULL COMMENT '播放时长',
  `view_time` datetime DEFAULT NULL COMMENT '播放时间',
  `viewerId` int(11) DEFAULT NULL COMMENT '观看者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_id` int(11) DEFAULT NULL COMMENT '创建人',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `modify_user_id` int(11) DEFAULT NULL COMMENT '修改人',
  `status` int(2) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `courseInfoId` (`courseInfoId`),
  KEY `videoCourseId` (`videoCourseId`),
  KEY `videoInfoId` (`videoInfoId`),
  KEY `lookerId` (`viewerId`),
  KEY `classifySubId` (`classifySubId`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COMMENT='视频观看日志表';

