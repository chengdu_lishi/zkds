 **# 职考大师在线考试培训学习练习系统** 

 **#### 介绍** 
职考大师是一款针对企业内部培训、成人职业考证培训的在线学习、练习、考试的系统。学习端口覆盖PC端、微信小程序、手机app，支持多种题型练习、视频内容学习。支持在线付费和激活码激活等多种支付方式。系统完善，一直在运营中，适合企业及教育机构。
现阶段将小程序端源码开放，剩余部分将陆续逐步开放，感谢大家支持


**#### 系统演示地址** 

 **PC端体验演示地址：http://exam.cdls666.com/ ** (之前域名因为备案问题重新更换了)

|  **小程序演示地址：**    | ![小程序测试地址](https://images.gitee.com/uploads/images/2019/1128/134034_2c86881b_5501620.jpeg "xcx.jpeg")    |
| --- | --- |



 



 **测试账号：15378184228 测试密码：123456** 

系统演示

#### 软件界面截图

![输入图片说明](https://images.gitee.com/uploads/images/2019/1128/122600_b88be690_5501620.png "zk1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/1128/122617_6c93611d_5501620.png "zk2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/1128/122639_81675b05_5501620.png "zk3.png")

| ![输入图片说明](https://images.gitee.com/uploads/images/2019/1128/122841_8023c106_5501620.jpeg "zk4.jpeg")    | ![输入图片说明](https://images.gitee.com/uploads/images/2019/1128/122912_9f360d50_5501620.jpeg "zk5.jpeg")    | ![输入图片说明](https://images.gitee.com/uploads/images/2019/1128/122927_5f5a1178_5501620.jpeg "zk6.jpeg")    |
| --- | --- | --- |



 **#### 联系我们** 
    
|  **项目交流QQ群：700325414**   |![输入图片说明](https://images.gitee.com/uploads/images/2019/1128/134952_6d5e2efa_5501620.jpeg "qq1.jpeg")     |
| --- | --- |
|   **商务合作微信：zgh168659700**    | ![输入图片说明](https://images.gitee.com/uploads/images/2019/1128/135017_381d718a_5501620.jpeg "wx.jpeg")  |

 
     

    
 


   