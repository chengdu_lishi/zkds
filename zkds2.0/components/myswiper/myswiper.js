// components/myswiper/myswiper.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    clientHeight: {
       type:Number,
       value: 0
    } ,
    objs : {
      type :Array,
      value:[]
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  }, 

  ready: function () {
    
  },

  /**
   * 组件的方法列表
   */
  methods: {
      
  }
})
