// components/refresh.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    hasMore: true,
    hasRefesh: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    loadMore: function (e) {
      var that = this;
      that.setData({
        hasRefesh: true,
      });
      if (!this.data.hasMore) return
    },

    refesh: function (e) {
      var that = this;
      that.setData({
        hasRefesh: true,
      });
    }
  }
})
