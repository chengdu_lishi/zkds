// pages/beikao/beikao.js
let app = getApp();
let wcache = require("../../utils/wcache.js")
let  rows = 10
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShow: true,
    currentTab: 0,
    kszxList: [],
    ksznList: []
  },

  tabNav: function (e) {
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      var showMode = e.target.dataset.current == 0;
      this.setData({
        currentTab: e.target.dataset.current,
      })
    }
  },

  //当页面改变是会触发
  bindchangeTag: function (e) {
    this.setData({
      currentTab: e.detail.current
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.page = 1, 
    this.pageMy = 1,
    this.getList(1)
    this.getList(2)
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    if (this.data.currentTab == 0) {
      this.page = 1;
      this.getList(1);
    } else {
      this.pageMy = 1;
      this.getList(2);
    }
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.currentTab == 0) {
      this.page++;
      this.getList(1);
    } else {
      this.pageMy++;
      this.getList(2);
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  getList(type) {
    app.httpUtils.postLoading(app.Api.LISTARTICLE, {
      page: type == 1 ? this.page : this.pageMy,
      rows: rows,
      classifySubId: wcache.get("classifySubId",""),
      type: type
    }).then(res => {
      wx.stopPullDownRefresh()
      if (res.code == '00') {
        let listtemp = [];
        if (type == 1) {
          if (this.page === 1) {
            listtemp = [];
          } else {
            listtemp = this.data.kszxList;
          }
        } else {
          if (this.pageMy === 1) {
            listtemp = [];
          } else {
            listtemp = this.data.ksznList;
          }
        }
        
        listtemp = listtemp.concat(res.data.root.map(item => {
          item.image = app.Api.getImageUrl(item.image);
          return item;
        }));

        if (type == 1) {
          this.setData({
            kszxList: listtemp
          })
        } else {
          this.setData({
            ksznList: listtemp
          })
        }
      }
    })
  },

  toDetail(e){
    wx.navigateTo({
      url: './detail/detail?id=' + e.currentTarget.dataset.id + "&name=" + e.currentTarget.dataset.name,
    }) 
  }
})