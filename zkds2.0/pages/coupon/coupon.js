// pages/coupon/coupon.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShow: true,
    currentTab: 0,
    couponList: [],
    couponList1: [],
    couponList2: [],
    usetype: -1,
    ishow :true
  },

  tabNav: function (e) {
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      var showMode = e.target.dataset.current == 0;
      this.setData({
        currentTab: e.target.dataset.current,
      })
    }
    this.getCouponList((parseInt(e.target.dataset.current)+1));
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.type = options.type;
    if (this.type == 1) {
      this.setData({ usetype: 1 })
      this.getCouponList(1)
      return;
    }
    this.getCouponList(1)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    if (this.data.currentTab == 0) {
      this.getCouponList(1)
    } else if (this.data.currentTab == 1) {
      this.getCouponList(2)
    } else if (this.data.currentTab == 2) {
      this.getCouponList(3)
    }
  },

  onCounponItem(e) {
    if (this.type == 1) {
      app.event.emit('coupon', e.currentTarget.dataset.obj);
      wx.navigateBack({})
    }
  },

  getCouponList(typeValue) {
    app.httpUtils.postLoading(app.Api.LISTCOUPON, {
      uid: app.globalData.userBean().id,
      type: typeValue,
    }).then(res => {
      wx.stopPullDownRefresh();
      if (res.code == '00') {
        let listtemp = [];
        listtemp = listtemp.concat(res.data);
        if (typeValue == 1) {
          this.setData({
            couponList: listtemp,
            ishow: listtemp.length <= 0
          })
        } else if (typeValue == 2) {
          this.setData({
            couponList1: listtemp,
            ishow: listtemp.length <= 0
          })
        } else if (typeValue == 3) {
          this.setData({
            couponList2: listtemp,
            ishow: listtemp.length <= 0
          })
        }
      }
    })
  }
})