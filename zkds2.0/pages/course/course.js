// pages/course/course.js
let app = getApp();
let wcache = require('../../utils/wcache.js')
let utils = require('../../utils/util.js')
var rows = 10;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShow: true,
    currentTab: 0,
    allVideoList: [],
    myVideoList: [],
    latelyVideo: null
  },


  tabNav: function (e) {
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      var showMode = e.target.dataset.current == 0;
      this.setData({
        currentTab: e.target.dataset.current,
      })
    }
  },

  //当页面改变是会触发
  bindchangeTag: function (e) {
    this.setData({
      currentTab: e.detail.current
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.page = 1, 
    this.pageMy = 1,
    this.listVideoCourse();
    this.getMyVideoList();

    app.event.on("loginData", this, (data) => {
      this.page = 1;
      this.pageMy = 1;
      this.listVideoCourse();
      this.getMyVideoList();
    })

    app.event.on("jilu", this, (data) => {
      this.getLastVideo();
    })

    //通过购买过来的
    app.event.on("buyvideo", this, res => {
      this.page = 1;
      this.pageMy = 1;
      this.listVideoCourse();
      this.getMyVideoList();
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.setNavigationBarTitle({
      title: wcache.get("course", {}).name,
    })

    this.getLastVideo();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    app.event.remove("jilu", this);
    app.event.remove("loginData", this);
    app.event.remove("buyvideo", this);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    if (this.data.currentTab == 0) {
      this.page = 1;
      this.listVideoCourse();
    } else {
      this.pageMy = 1;
      this.getMyVideoList();
    }
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.currentTab == 0) {
      this.page++;
      this.listVideoCourse();
    } else {
      this.pageMy++;
      this.getMyVideoList();
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  listVideoCourse() {
    let classifySubId = wcache.get("classifySubId", "");
    app.httpUtils.postLoading(app.Api.LISTVIDEOCOURSEHTTP, {
      page: this.page,
      rows: rows,
      uid: app.globalData.userBean().id,
      classifySubId: classifySubId,
      condition: ''
    }).then(res => {
      wx.stopPullDownRefresh();
      if (res.code == '00') {
        let listtemp = [];
        if (this.page === 1) {
          listtemp = [];
        } else {
          listtemp = this.data.allVideoList;
        }
        listtemp = listtemp.concat(res.data.rows.map(item => {
          item.image = app.Api.getImageUrl(item.image);
          return item;
        }));
        this.setData({
          allVideoList: listtemp
        })
      }
      wx.stopPullDownRefresh();
    })
  },

  getMyVideoList() {
    app.httpUtils.postLoading(app.Api.LISTMYVIDEO, {
      page: this.pageMy,
      rows: rows,
      uid: app.globalData.userBean().id,
    }).then(res => {
      wx.stopPullDownRefresh();
      if (res.code == '00') {
        let listtemp = [];
        if (this.pageMy === 1) {
          listtemp = [];
        } else {
          listtemp = this.data.myVideoList;
        }
        listtemp = listtemp.concat(res.data.rows.map(item => {
          item.image = app.Api.getImageUrl(item.image);
          return item;
        }));
        this.setData({
          myVideoList: listtemp
        })
      }
    })
  },

  getLastVideo() {
    app.httpUtils.post(app.Api.GETLASTVIDEO, {
      classifySubId: wcache.get("classifySubId", ""),
      uid: app.globalData.userBean().id,
    }).then(res => {
      if (res.code == '00') {
        var latelyVideo = res.data;
        latelyVideo.jixustr = "上次观看《" + latelyVideo.title + "》" + utils.formatSeconds(latelyVideo.playDuration );
        this.setData({
          latelyVideo: latelyVideo
        })
      } else {
        this.setData({
          latelyVideo: null
        })
      }
    })
  }

  ,
  toVideo(e) {
    let id = e.currentTarget.dataset.obj.id
    wx.navigateTo({
      url: './video/video?id=' + id,
    })
  },

  jixulook() {
    wx.navigateTo({
      url: './video/video?id=' + this.data.latelyVideo.videoCourseId + "&mtype=1" + "&playDuration=" + this.data.latelyVideo.playDuration + "&url="+this.data.latelyVideo.url,
    })
  },

  /**
   * 切换科目
   */
  selectSubject() {
    wx.navigateTo({
      url: '../selectsubject/selectsubject?type=2',
    })
  }

})