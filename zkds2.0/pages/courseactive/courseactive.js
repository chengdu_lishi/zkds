// pages/courseactive/courseactive.js
let app = getApp();
let utils = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    new app.WeToast();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  activeInput(e) {
    this.activecode = e.detail.value;
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  courseActive() {
    if (utils.isEmpty(this.activecode)) {
      this.wetoast.toast({
        title: "请输入激活号"
      })
      return;
    }
    app.httpUtils.postLoading(app.Api.ACTIVE, {
      uid: app.globalData.userBean().id,
      code: this.activecode,
    }).then(res => {
      if (res.code == '00') {
        app.event.emit("loginData", {})
      }
      this.wetoast.toast({
        title: res.msg
      })
    })
  }
})