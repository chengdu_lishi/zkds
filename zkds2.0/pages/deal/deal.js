// pages/deal/deal.js
let app =getApp();
let wxcache = require("../../utils/wcache.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentIndex:0,
    navlist: ["待解决", "已解决"],
    testInfoList: null, //这个表示章节联系
    testList: null,  
    t : ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.page = 1;
    this.rows = 10;
    this.t = options.type;
    this.setData({
      t: this.t
    })
  },

  selectNav(e) {
    this.setData({
      currentIndex: e.currentTarget.dataset.index,
    })
    this.onPullDownRefresh();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.onPullDownRefresh();
  },

  onPullDownRefresh() {
    this.page = 1;
    this.getErrorList()
  },

  onReachBottom() {
    this.page++
    this.getErrorList()
  },

  getErrorList() {
    let url = app.Api.LISTERRORTEST;
    let courseInfoId = wxcache.get("courseInfoId", "");
    app.httpUtils.postLoading(url, {
      page: this.page,
      rows: this.rows,
      courseInfoId: courseInfoId,
      uid: app.globalData.userBean().id,
      type: this.t,
      isSolve: this.data.currentIndex
    }).then(res => {
      if (res.code == '00') {
        if (this.t == "01" || this.t == '02' || this.t == '03') {
          let listtemp = [];
          if (this.page == 1) {
            listtemp = [];
          } else {
            listtemp = this.data.testInfoList;
          }
          listtemp = listtemp.concat(res.data.root);
          this.setData({
            testInfoList: listtemp
          })
        } else {
          let listtemp = [];
          if (this.page === 1) {
            listtemp = [];
          } else {
            listtemp = this.data.testList;
          }
          if (res.data.root != null) {
            for (let i = 0; i < res.data.root.length; i++) {
              listtemp = listtemp.concat(res.data.root[i].children);
            }
          }
          this.setData({
            testList: listtemp
          })
        }
      }
      wx.stopPullDownRefresh();
    })
  },


  bindTest(e) {
    let id = e.currentTarget.dataset.id;
    let obj = e.currentTarget.dataset.obj;
    this.getError(obj, id);
  },

  /**
   * 获取错题巩固的列表
   */
  getError(obj, testId) {
    let url = app.Api.LISTERROR;
    app.httpUtils.postLoading(url, {
      uid: app.globalData.userBean().id,
      testId: testId,
      isSolve: this.data.currentIndex
    }).then(res => {
      if (res.code == '00') {
        obj.questions = res.data;
        wxcache.put("ti", obj);
        wx.navigateTo({
          url: '../zuoti/zuoti?type=7'
        })
      }
    })
  } 
})