// pages/errorcement/errorcement.js
let page = 1;
let rows = 10;
let app = getApp();
let wxcache = require("../../utils/wcache.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentTab: 0,
    testInfoList: null, //这个表示章节联系
    testList: null,  
    modeType : 1 , //1.表示错题巩固 2.表示收藏题目
    solve: 0, 
    noSolve:0 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.page = 1;
    this.rows = 10;
    this.t = "01"
    this.setData({ modeType: options.type})
    if(options.type == 1){
        wx.setNavigationBarTitle({
          title: '错题巩固',
        })
    }else if(options.type == 2){
      wx.setNavigationBarTitle({
        title: '收藏题目',
      })
    }
  },

  onShow(){
    this.onPullDownRefresh();
    this.getErrorNumber();
  } 
,
  onPullDownRefresh() {
    this.page = 1;
    this.getErrorList()
  },

  onReachBottom() {
    this.page++
    this.getErrorList()
  },

  tabNav: function (e) {
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      var showMode = e.target.dataset.current == 0;
      this.setData({
        currentTab: e.target.dataset.current,
      })
    }
    var mode = this.data.currentTab
    if (mode == 0) {
      this.t = "01"
    } else if (mode == 1) {
      this.t = "02"
    } else if (mode == 2) {
      this.t = "03"
    } else if (mode == 3) {
      this.t = "05"
    }
    this.onShow();
  },

  //当页面改变是会触发
  bindchangeTag: function (e) {
    this.setData({
      currentTab: e.detail.current
    })
  },

  getErrorNumber(){
    if (this.data.modeType == 1){
      let courseInfoId = wxcache.get("courseInfoId", "");
      app.httpUtils.postLoading(app.Api.COUNTERROR, {
        courseInfoId: courseInfoId,
        uid: app.globalData.userBean().id,
        type: this.t
      }).then(res =>{
          if(res.code == "00"){
            this.setData({
              solve: res.data.solve,
              noSolve:res.data.noSolve
            })
        }
      })
    }
  },

  getErrorList() {
    var url = ""
    if(this.data.modeType == 1 ){
      url = app.Api.LISTERRORTEST;
    } else{
      url = app.Api.LISTCOLLECTTEST;
    }
    let courseInfoId = wxcache.get("courseInfoId", "");
    app.httpUtils.postLoading(url, {
      page: this.page,
      rows: this.rows,
      courseInfoId: courseInfoId,
      uid: app.globalData.userBean().id,
      type: this.t,
      isSolve: 0
    }).then(res => {
      if (res.code == '00') {
        if (this.t == "01" ||this.t == '02' || this.t == '03'){
          let listtemp = [];
          if (this.page == 1) {
            listtemp = [];
          } else {
            listtemp = this.data.testInfoList;
          }
          listtemp = listtemp.concat(res.data.root);
          this.setData({
            testInfoList: listtemp
          })
        }else{
          let listtemp = [];
          if (this.page === 1) {
            listtemp = [];
          } else {
            listtemp = this.data.testList;
          }
          if (res.data.root != null) {
            for (let i = 0; i < res.data.root.length; i++) {
              listtemp = listtemp.concat(res.data.root[i].children);
            }
          }
          this.setData({
            testList: listtemp
          })
        }
      }
      wx.stopPullDownRefresh();
    })
  },

  bindTest(e){
    let id = e.currentTarget.dataset.id;
    let obj = e.currentTarget.dataset.obj;
    this.getError(obj,id);
  },

  /**
   * 获取错题巩固的列表
   */
  getError(obj,testId){
    let url = ""
    if (this.data.modeType == 1){
      url = app.Api.LISTERROR;
    }else{
      url = app.Api.LISTCOLLECT;
    }
    app.httpUtils.postLoading(url,{
      uid: app.globalData.userBean().id,
      testId: testId,
      isSolve:0
    }).then(res=>{
      if(res.code=='00'){
        obj.questions = res.data;
        wxcache.put("ti",obj);
        wx.navigateTo({
          url: '../zuoti/zuoti?type=7'
        })
      }
    })
  },

  toDeal(){
    wx.navigateTo({
      url: '../deal/deal?type='+this.t,
    })
  }

})