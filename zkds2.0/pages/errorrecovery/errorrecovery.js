// pages/errorrecovery/errorrecovery.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    textValue: "",
    typeList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.qid = options.qid;
    this.getErrorList();
    new app.WeToast();
  },

  userInput(e) {
    this.setData({
      textValue: e.detail.value
    })
  },

  getErrorList() {
    app.httpUtils.postLoading(app.Api.GETERRORTYPE, {}).then(res => {
      if (res.code == '00') {
        this.setData({
          typeList: res.data
        })
      }
    })
  },

  selectMode(e) {
    let index = e.currentTarget.dataset.index;
    let typeObj = this.data.typeList[index];
    if (typeObj.isSelect) {
      typeObj.isSelect = false
    } else {
      typeObj.isSelect = true
    }
    this.data.typeList[index] = typeObj;
    this.setData({
      typeList: this.data.typeList
    })
  },

  getTypeString() {
    var types = []
    for (let i = 0; i < this.data.typeList.length; i++) {
      let obj = this.data.typeList[i];
      types.push(obj.code);
    }
    return types.join(",");
  },

  submitError() {
    app.httpUtils.postLoading(app.Api.SUBMITERROR, {
      qid: this.qid,
      uid: app.globalData.userBean().id,
      type: this.getTypeString(),
      content: this.data.textValue
    }).then(res => {
      if (res.code == '00') {
        wx.navigateBack({})
      }else{
        this.wetoast.toast({
          title: res.msg,
          duration: 1000
        })
      }
    })
  }
})