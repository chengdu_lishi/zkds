// pages/exerciselist/exerciselist.js
let app = getApp();
let wcache = require('../../utils/wcache.js')
let page = 1;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    modelist: [{ "name": "正常模式" }, { "name": "预览模式" }],
    testInfolist: [],
    isbuy: false,
    isRedo: 0,
    mode : 1 ,
    testId: "",
    actionSheetItems: ["顺序模式", "浏览模式", "组卷模式"],
    type:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var mode = 1;
    //考前密押和历年真题
    // if (options.type == '03' || options.type == '05'){
    //    mode = 3;
    // }
    this.type = options.type;
    this.setData({
      type :this.type,
      mode: mode
    })
    app.event.on("tryagain", this, data => {
      if (data == -1) {
        this.setData({ isRedo: 0, testId: "" })
      } else {
        this.setData({ isRedo: 1, testId: data })
      }
    })

    app.event.on("jiaojuan", this, data => {
      this.isShow = data;
    })

    if (options.type == '03' ){
       wx.setNavigationBarTitle({
         title: '历年真题',
       })
    } else if(options.type == '05'){
      wx.setNavigationBarTitle({
        title: '考前密押',
      })
    }else {
      wx.setNavigationBarTitle({
        title: '预测演练',
      })
    }
  },

  onShow: function () {
    page = 1;
    let jisojusn = wcache.get("jiaojuan", null);
    if (jisojusn != null) {
      this.jiaojuan(jisojusn);
    } else {
      this.getTestList();
    }
    if (this.isShow) {
      this.isShow = false;
      wx.navigateTo({
        url: '/pages/zuoti/report/report',
      })
    }
  },

  jiaojuan(json) {
    app.httpUtils.postJsonLoading(app.Api.FINISH, JSON.stringify(json)).then(res => {
      wcache.remove("jiaojuan");
      page = 1;
      this.getTestList();
    })
  },

  onUnload() {
    app.event.remove("tryagain", this);
    app.event.remove("jiaojuan", this);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    page = 1;
    this.getTestList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    page++;
    this.getTestList();
  },

  getTestList() {
    app.httpUtils.postLoading(app.Api.LISTTESTINFO, {
      uid: app.globalData.userBean().id,
      courseInfoId: wcache.get("courseInfoId", ""),
      type: this.type,
      rows: 10,
      page: page,
    }).then(res => {
      if (res.code == '00') {
        let listtemp = [];
        if (page === 1) {
          listtemp = [];
          let isbuy = false;
          if (res.data.rows != null && res.data.rows.length > 0) {
            isbuy = res.data.rows[0].isBuy > 0
            this.setData({
              isbuy: isbuy
            })
          }
        } else {
          listtemp = this.data.testInfolist;
        }

        if (res.data.rows != null) {
          for (let i = 0; i < res.data.rows.length; i++) {
            listtemp = listtemp.concat(res.data.rows[i].children);
          }
        }

        this.setData({
          testInfolist: listtemp
        })
        wx.stopPullDownRefresh();

        //再做一次
        if (this.data.isRedo == 1) {
          this.startTest(this.data.testId)
        }
      }
    })
  },

  bindTest(e) {
    let id = e.currentTarget.dataset.id;
    let obj = e.currentTarget.dataset.obj;
    if (obj.questionNumber <= 0) {
      wx.showToast({
        title: "没有题目",
      })
      return;
    }
    if (obj.allowTry == "是" || this.data.isbuy) {
      if(this.type == '03' && this.data.mode == 1){
        let count =  obj.answerNumber == undefined ? 0 : obj.answerNumber 
        if (count > 0){
          wx.showModal({
            title: '温馨提示',
            content: '是否回到上次做题记录',
            confirmText:"重新做题",
            cancelText:"继续做题",
            success: res => {
              if(res.cancel){
                this.startTest(id);
              } else if (res.confirm){
                this.setData({ isRedo: 1 })
                this.startTest(id);
              }
            }
          })
        }else {
          this.startTest(id);
        }
      }else{
        this.startTest(id);
      }
    } else {
      // wx.navigateTo({
      //   url: '../buy/buy?type=1',
      // })
      wx.navigateTo({
        url: '../courseactive/courseactive',
      })
    }
  }
  ,
  //开始连题
  startTest(id) {
    app.httpUtils.postLoading(app.Api.STARTTEST, {
      testId: id,
      model: this.data.mode,
      uid: app.globalData.userBean().id,
      isRedo: this.data.isRedo,
    }).then(res => {
      //还原数据
      this.setData({ isRedo: 0, testId: "" })
      if (res.code == "00") {
        if (res.data.questions.length > 0) {
          wcache.put("ti", res.data);
          wx.navigateTo({
            url: '../zuoti/zuoti?type=' + this.data.mode
          })
        } else {
          wx.showToast({
            title: "没有题目",
          })
        }
      } else {
        wx.showToast({
          title: res.msg,
        })
      }
    })
  },

   /**
   * 选择模式
   */
  selectMode() {
    wx.showActionSheet({
      itemList: this.data.actionSheetItems,
      success: res => {
        this.setData({
          mode: res.tapIndex + 1
        })
      }
    })
  },

   radioChange(e) {
    this.setData({
      mode: e.detail.value
    })
  }
})