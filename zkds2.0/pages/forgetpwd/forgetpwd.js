// pages/register/register.js
let app = getApp();
let utils = require("../../utils/util.js")
var interval = null //倒计时函数
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentInput: 0,
    disabled: false,
    time: '获取验证码', //倒计时 
    currentTime: 60,
    msgCode:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    new app.WeToast();
    wx.setNavigationBarTitle({
      title: '忘记密码',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  accountfocus() {
    this.setData({ currentInput: 0 })
  },

  yzmfocus() {
    this.setData({ currentInput: 1 })
  },

  pwdfocus() {
    this.setData({ currentInput: 2 })
  },

  accountInput(e) {
    this.account = e.detail.value;
  },

  yzmInput(e) {
    this.yzm = e.detail.value;
  },

  pwdInput(e) {
    this.pwd = e.detail.value;
  },

  alertpwd() {
    if (utils.isEmpty(this.account)) {
      this.wetoast.toast({
        title: "请输入手机号"
      })
      return;
    }
    if (utils.isEmpty(this.yzm)) {
      this.wetoast.toast({
        title: "请输入验证码"
      })
      return;
    }
    if (utils.isEmpty(this.pwd)) {
      this.wetoast.toast({
        title: "请输入密码"
      })
      return;
    }

    app.httpUtils.postLoading(app.Api.SETPASSWORD, {
      phone: this.account,
      password:this.pwd,
      code: this.yzm
    }).then(res => {
      this.wetoast.toast({
        title: res.msg
      })
      if(res.code == '00'){
        setTimeout(res=>{
          wx.navigateBack({})
        },500)
        
      }
    })
  },

  getCode: function (options) {
    var that = this;
    var currentTime = that.data.currentTime
    interval = setInterval(function () {
      currentTime--;
      that.setData({
        time: currentTime + '秒'
      })
      if (currentTime <= 0) {
        clearInterval(interval)
        that.setData({
          time: '重新发送',
          currentTime: 60,
          disabled: false
        })
      }
    }, 1000)
  },

  getVerificationCode() {
    if (utils.isEmpty(this.account)) {
      this.wetoast.toast({
        title: "请输入手机号"
      })
      return;
    }
    if (this.data.disabled == true) { return }
    this.getCode();
    var that = this
    that.setData({
      disabled: true
    })
    
    app.httpUtils.get(app.Api.SENDCODE, {
      phone: this.account
    }).then(res => {
      this.setData({
        msgCode: res.msg.replace(/[^0-9]/ig, "")
      })
      this.yzm = this.data.msgCode;
      wx.setStorageSync('SESSID', res.data)
      this.wetoast.toast({
        title: res.msg
      })
    })
  },

})