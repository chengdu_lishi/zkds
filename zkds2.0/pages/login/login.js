// pages/login/login.js
let app = getApp();
let utils =require("../../utils/util.js") 
let wcache =require("../../utils/wcache.js")
Page({
  /**
   * 页面的初始数据
   */
  data: {
    currentInput:0,
    modeType :0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    new app.WeToast();
    this.data.modeType = utils.isEmpty(options.type) ? 0 : options.type;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  accountfocus(){
    this.setData({ currentInput: 0 })
  },

  pwdfocus(){
    this.setData({ currentInput: 1 })
  },

  accountInput(e){
    this.account = e.detail.value;
  },

  pwdInput(e) {
    this.pwd = e.detail.value;
  },

  login:function(){
    if (utils.isEmpty(this.account)){
      this.wetoast.toast({
        title: '请输入账号',
        duration: 1000
      })
      return;
    }

    if (utils.isEmpty(this.pwd)) {
      this.wetoast.toast({
        title: '请输入密码',
        duration: 1000
      })
      return;
    }

    app.httpUtils.postLoading(app.Api.LOGIN,{
      phone: this.account,
      password:this.pwd
    }).then(res => {
      if(res.code === '00'){
        wcache.put('user', res.data);
        wcache.put('loginToken',res.data.loginToken)
        if (this.data.modeType == 0){
          app.event.emit("loginData",{})
          wx.navigateBack({})
        }else{
          wx.redirectTo({
            url: '../selectsubject/selectsubject',
          })  
        }
      }else{
        this.wetoast.toast({
          title: res.msg,
          duration: 1000
        })
      }
    })
  }
})