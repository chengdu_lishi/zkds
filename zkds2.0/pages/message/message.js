// pages/message/message.js
let app = getApp();
var page = 1, rows = 10;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    messageList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '消息中心',
    })
    this.getMessageList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    page = 1;
    this.getMessageList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    page++;
    this.getMessageList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  getMessageList() {
    app.httpUtils.postLoading(app.Api.LISTMESSAGE, {
      page: page,
      rows: rows,
      uid: app.globalData.userBean().id
    }).then(res => {
      let listtemp = [];
      if (page === 1) {
        listtemp = [];
      } else {
        listtemp = this.data.messageList;
      }
      listtemp = listtemp.concat(res.data.root);
      this.setData({
        messageList: listtemp
      })
      wx.stopPullDownRefresh();
    })
  },

  toMessgeDetail(e) {
    let id = e.currentTarget.dataset.id;
    let index = e.currentTarget.dataset.index;
    this.data.messageList[index].isRead = 1;
    this.setData({
      messageList: this.data.messageList
    })
    this.isRead(id);
  },

  isRead(id) {
    app.httpUtils.postLoading(app.Api.SETREAD, {
      "ids[]": id,
      uid: app.globalData.userBean().id
    }).then(res => {

    })
  },

  allread() {
    var arr = new Array();
    this.setData({
      messageList: this.data.messageList.map(item => {
        item.isRead = 1;
        arr.push(item.id);
        return item;
      })
    })
    var str = arr.join(",");
    console.log(str);
    this.isRead(str)
  }
})