// pages/monikaochang/monikaochangstart/monikaochangstart.js
let app = getApp()
let wcache = require("../../../utils/wcache.js")

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  starttest() {
    let uid = app.globalData.userBean().id;
    let courseInfoId = wcache.get("courseInfoId", "");
    app.httpUtils.postLoading(app.Api.STARTEXAM, {
      uid: uid,
      courseInfoId: courseInfoId,
    }).then(res => {
      if (res.code == "00") {
        //用于报表界面的时候
        res.data.isShowScore = true
        wcache.put("ti", res.data);
        wx.redirectTo({
          url: '../../zuoti/zuoti?type=6'
        })
        // wx.navigateTo({
        //   url: '../random/random',
        // })
      } else if (res.code == "04") {
        this.jiaojuan(res.data.id);
      }else{
        wx.showToast({
          title: res.msg,
        })
      }
    })
  },

  //交卷
  jiaojuan(id) {
    let json = {eid: id,questions: []}
    app.httpUtils.postJsonLoading(app.Api.FINISH, JSON.stringify(json)).then(res => {
      this.starttest();
    })
  },

  looklist(){
    wx.navigateTo({
      url: '../randomlist/randomlist',
    })
  }
})