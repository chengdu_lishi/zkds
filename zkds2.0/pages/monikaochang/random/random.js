// pages/monikaochang/random/random.js
let app  =getApp();
let wcache = require("../../../utils/wcache.js")
Page({
  /**
   * 页面的初始数据
   */
  data: {
      testInfo:null
  },  

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      testInfo: wcache.get("ti", null)
    })
  },

  starttest() {
    wx.redirectTo({
      url: '../../zuoti/zuoti?type=6'
    })
  }
})