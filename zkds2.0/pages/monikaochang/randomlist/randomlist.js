// pages/monikaochang/randomlist/randomlist.js
let app = getApp();
let wcache = require("../../../utils/wcache.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    testList: [],
    actionSheetItems:["重新做题","查看解析"],
    lock:false
  },

  onShow: function () {
    this.getExamList()
  },

  getExamList() {
    let uid = app.globalData.userBean().id;
    let courseInfoId = wcache.get("courseInfoId", "");
    app.httpUtils.postLoading(app.Api.LISTEXAM, {
      uid: uid,
      courseInfoId: courseInfoId,
    }).then(res => {
      if (res.code == '00') {
        this.setData({
          testList: res.data
        })
      }
    })
  },

  getQusetionsList(testid, mode){
    app.httpUtils.postLoading(app.Api.RESITEXAM,{
      id:testid,
      type: mode
    }).then(res => {
      if(res.code == '04'){
        this.jiaojuan(res.data.id, testid ,mode)
      }else{
        res.data.isShowScore = true
        wcache.put('ti',res.data);
        if(mode==1){
          wx.navigateTo({
            url: '../../zuoti/zuoti?type=6'
          })
        }else if(mode == 2){
          wx.navigateTo({
            url: '../../zuoti/zuoti?type=2'
          })
        }
      }
    })
  },

    //交卷
  jiaojuan(id , testid ,mode) {
    let json = { eid: id, questions: [] }
    app.httpUtils.postJsonLoading(app.Api.FINISH, JSON.stringify(json)).then(res => {
      this.getQusetionsList(testid,mode);
    })
  },

  showMode(e){
    if (this.data.lock){return}
    let id = e.currentTarget.dataset.id;
    wx.showActionSheet({
      itemList: this.data.actionSheetItems,
      success: res => {
        this.getQusetionsList(id, res.tapIndex + 1)
      }
    })
  },

  deleteTest(e){
    this.data.lock = true;
    wx.showModal({
      title: '温馨提示',
      content: '是否确认删除',
      success: res=>{
        console.log(res)
        if (!res.cancel){
          let id = e.currentTarget.dataset.id;
          app.httpUtils.postLoading(app.Api.DELETEEXAM, {
            examId: id,
          }).then(res => {
            if (res.code == '00') {
              this.getExamList();
            } else{
              wx.showToast({
                title: res.msg,
              })
            }
          })
        }
        this.data.lock = false;
      }
    })
  }

})