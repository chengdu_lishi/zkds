// pages/mynote/mynote.js

let page = 1;
let rows = 10;
let wxcache = require("../../utils/wcache.js")
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentTab: 0,
    testList: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.t = "01"
    wx.setNavigationBarTitle({
      title: '我的笔记',
    })
  },

  onShow: function () {
    page = 1;
    this.getNoteList();
  },

  tabNav: function (e) {
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      var showMode = e.target.dataset.current == 0;
      this.setData({
        currentTab: e.target.dataset.current,
      })
    }
    var mode = this.data.currentTab
    if (mode == 0) {
      this.t = "01"
    } else if (mode == 1) {
      this.t = "02"
    } else if (mode == 2) {
      this.t = "03"
    } else if (mode == 3) {
      this.t = "05"
    }
    this.onShow();
  },

  //当页面改变是会触发
  bindchangeTag: function (e) {
    this.setData({
      currentTab: e.detail.current
    })
  },

  onPullDownRefresh() {
    this.onShow();
  }
  ,
  onReachBottom() {
    page++;
    this.getNoteList();
  }
  ,

  startest(e){
    let obj = e.currentTarget.dataset.obj;
    let test ={
      currentQuestionOrder: obj.questionId ,
      questions: this.data.testList
    }
    console.log(test)
    wxcache.put('ti',test);
    wx.navigateTo({
      url: '../zuoti/zuoti?type=8'
    })
  },

  getNoteList() {
    let courseInfoId = wxcache.get("courseInfoId", "");
    app.httpUtils.postLoading(app.Api.NOTELIST, {
      page: page,
      rows: rows,
      courseInfoId: courseInfoId,
      uid: app.globalData.userBean().id,
      type: this.t
    }).then(res => {
      let listtemp = [];
      if (page === 1) {
        listtemp = [];
      } else {
        listtemp = this.data.testList;
      }
      listtemp = listtemp.concat(res.data.root.map(item=>{
        item.orders = item.questionId;
        return item;
      }));
      this.setData({
        testList: listtemp
      })
    })
  }
})