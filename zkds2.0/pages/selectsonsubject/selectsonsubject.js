// pages/selctsonsubject/selectsonsubject.js
let app = getApp();
var page =1,rows=10;
let wecache = require("../../utils/wcache.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    classList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.mode = options.type;
    wx.setNavigationBarTitle({
      title: options.title,
    })
    this.pid = options.pid;
    this.cid= options.cid;
    this.getSujects();
  },

  toMain(e){
    wecache.put("course", e.currentTarget.dataset.obj)
    wecache.put("courseInfoId", e.currentTarget.dataset.id + "");
    wecache.put("classifyId", this.pid);
    wecache.put("classifySubId", this.cid);
    if (this.mode == 0 || this.mode == 1){
      wx.reLaunch({
        url: '../tiku/tike',
      }) 
    } else if (this.mode == 2){
      wx.reLaunch({
        url: '../course/course',
      }) 
    } else if(this.mode == 3) {
      wx.reLaunch({
        url: '../we/we',
      }) 
    }
  },

  getSujects(){
    var that  = this;
    app.httpUtils.postLoading(app.Api.LISTCOURSEINFO,{
      page:page,
      rows:rows,
      classifyId:this.pid,
      classifySubId:this.cid
    }).then(res=>{
      wx.stopPullDownRefresh();
      if(res.code == '00'){
        let listtemp = [];
        if(page ===1){
          listtemp= [];
        }else{
          listtemp = this.data.classList;
        }
        listtemp = listtemp.concat(res.data.rows);
        this.setData({
          classList: listtemp
        })
      }
    })
  },

 
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
      page =1;
      this.getSujects();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
      page =page +1;
      this.getSujects();
  },
})