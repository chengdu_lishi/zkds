// pages/selectsubject/selectsubject.js

var app = getApp();
let util = require("../../utils/util.js")
let wecache =require("../../utils/wcache.js")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    subjects:[],
    selectIndex:0,
    mode : 0 
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var mode  = (options.type == undefined) ? 0 : options.type;
    this.setData({ mode: mode})
    let courseInfoId = wecache.get("courseInfoId","");
    let classifyId = wecache.get("classifyId","");
    let classifySubId = wecache.get("classifySubId","");
    if (mode == 0 && (!util.isEmpty(courseInfoId)) && (!util.isEmpty(classifyId)) && (!util.isEmpty(classifySubId)) ){
        wx.switchTab({
          url: '../tiku/tike',
        })
    }else{
      wx.setNavigationBarTitle({
        title: '选择科目',
      })
      new app.WeToast();
      this.getKeMu();
    }
  },

  selectSubject(event){
    var index = event.currentTarget.dataset['index'];
    this.setData({
      selectIndex: index
    })
  },

  /**
   * 获取科目列表
   */
  getKeMu(){
    app.httpUtils.getLoading(app.Api.LISTCLASSIFY, {}).then(res => {
      if (res.code === '00') {
        this.setData({subjects:res.data.map(item=>{
            item.pic = app.Api.getImageUrl(item.pic);
            return item;
        }),
          selectIndex: 0
        })
      } else {
        this.wetoast.toast({
          title: res.msg
        })
      }
    });
  },
})