// pages/studystatistics/studystatistics.js
let app = getApp();
let wcache =require("../../utils/wcache.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userBean:null,
    tongji:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: wcache.get("course", {}).name,
    })
    var user = app.globalData.userBean(); 
    user.headPic = app.Api.getImageUrl(user.headPic);
    this.setData({
      userBean: user,
      name: wcache.get("course",{}).name
    })
    app.httpUtils.postLoading(app.Api.GETUSERSCORE,{
      uid: app.globalData.userBean().id,
      courseInfoId: wcache.get("courseInfoId","")
    }).then(res => {
        if(res.code=='00'){
          this.setData({
            tongji:res.data
          })
        }
    })
  },
})