// pages/tiku/tike.js
var event = require('../../utils/event.js')
let wcache = require('../../utils/wcache.js')
let app = getApp();
var rows = 10
Page({

  /**
   * 页面的初始数据
   */
  data: {
    moduleItem: [
      { name: "章节练习", icon: "../../images/ic_tike_zjlx.png" },
      { name: "预测演练", icon: "../../images/ic_tike_yclx.png" },
      { name: "历年真题", icon: "../../images/ic_tike_lnzt.png" },
      { name: "模拟考场", icon: "../../images/ic_tike_mnkc.png" },
      { name: "考前密押", icon: "../../images/ic_tike_kqmy.png" },
      { name: "错题巩固", icon: "../../images/ic_tike_ctgg.png" },
      { name: "收藏题目", icon: "../../images/ic_tike_sctm.png" },
      { name: "我的笔记", icon: "../../images/ic_tike_wdbj.png" }
    ],
    adv: [],
    testInfoBean: null, //是否有继续做题的功能
    testEssence: [],
    isRedo: 0,
    testId: "",
    swiperIndex: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.page = 1;
    this.getBanner();
    this.listTestEssence();
    app.event.on("loginData", this, (data) => {
      this.page = 1;
      this.listTestEssence();
    })

    app.event.on("buytiku", this, data => {
      this.page = 1;
      this.listTestEssence();
    })

    app.event.on("tryagain", this, data => {
      //还原不让进入在做一次
      if(data == -1){
        this.setData({ isRedo: 0, testId: "" })  
      }else{
        this.setData({ isRedo: 1, testId: data })
      }
    })

    app.event.on("jiaojuan", this, data => {
      this.isShow = data;
    })
  },

  swiperChange(e) {
    const that = this;
    that.setData({
      swiperIndex: e.detail.current,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.setNavigationBarTitle({
      title: wcache.get("course", {}).name,
    })

    let jisojusn = wcache.get("jiaojuan", null);
    if (jisojusn != null) {
      this.jiaojuan(jisojusn);
    } else {
      this.getLastExercise();
    }

    //跳转报告界面
    if (this.isShow) {
      this.isShow = false;
      wx.navigateTo({
        url: '/pages/zuoti/report/report',
      })
    }
  },

  /**
   * 点击继续做题的时候使用
   */
  startTest() {
    app.httpUtils.postLoading(app.Api.GETEXERCISE, {
      id: this.data.testInfoBean.id,
      uid: app.globalData.userBean().id,
      isRedo: this.data.isRedo,
    }).then(res => {
      //还原数据
      this.setData({ isRedo: 0, testId: "" })
      if (res.code == "00") {
        if (res.data.questions.length > 0) {
          wcache.put("ti", res.data);
          wx.navigateTo({
            url: '../zuoti/zuoti?type=' + this.data.testInfoBean.model
          })
        } else {
          wx.showToast({
            title: "没有题目",
          })
        }
      } else {
        wx.showToast({
          title: res.msg,
        })
      }
    })
  },

  //交卷
  jiaojuan(json) {
    app.httpUtils.postJsonLoading(app.Api.FINISH, JSON.stringify(json)).then(res => {
      wcache.remove("jiaojuan");
      this.getLastExercise();
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    app.event.remove("loginData", this)
    app.event.remove("buytiku", this);
    app.event.remove("tryagain", this);
    app.event.remove("jiaojuan", this);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.page = 1;
    this.getLastExercise();
    this.getBanner();
    this.listTestEssence();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.page++;
    this.listTestEssence();
  },

  getBanner: function () {
    let courseInfoId = wcache.get("courseInfoId", "");
    let classifyId = wcache.get("classifyId", "");
    let classifySubId = wcache.get("classifySubId", "");

    app.httpUtils.post(app.Api.GETBANNER, {
      classifyId: classifyId,
      classifySubId: classifySubId,
      courseInfoId: courseInfoId
    }).then(res => {
      this.setData({
        adv: res.data.map(item => {
          item.image = app.Api.getImageUrl(item.image);
          return item;
        })
      })
    })
  },

  /**
   * 获取当前的进度
   */
  getLastExercise: function () {
    let uid = app.globalData.userBean().id;
    let courseInfoId = wcache.get("courseInfoId", "");
    app.httpUtils.post(app.Api.GETLASTEXERCISE, {
      uid: uid,
      courseInfoId: courseInfoId
    }).then(res => {
      if (res.code == '00') {
        this.setData({ testInfoBean: res.data })
      }

      //再做一次
      if (this.data.isRedo == 1) {
        this.startTest1(this.data.testId)
      }
    })
  },

  //**在做一次的时候调用 */
  startTest1(id) {
    app.httpUtils.postLoading(app.Api.STARTTEST, {
      testId: id,
      model: this.data.testInfoBean.model,
      uid: app.globalData.userBean().id,
      isRedo: this.data.isRedo,
    }).then(res => {
      //还原数据
      this.setData({ isRedo: 0, testId: "" })
      if (res.code == "00") {
        if (res.data.questions.length > 0) {
          wcache.put("ti", res.data);
          wx.navigateTo({
            url: '../zuoti/zuoti?type=' + this.data.testInfoBean.model
          })
        } else {
          wx.showToast({
            title: "没有题目",
          })
        }
      } else {
        wx.showToast({
          title: res.msg,
        })
      }
    })
  },

  listTestEssence: function () {
    let uid = app.globalData.userBean().id;
    let courseInfoId = wcache.get("courseInfoId", "");
    app.httpUtils.post(app.Api.LISTTESTESSENCE, {
      page: this.page,
      rows: rows,
      uid: uid,
      courseInfoId: courseInfoId
    }).then(res => {
      if (res.code == '00') {
        let listtemp = [];
        if (this.page === 1) {
          listtemp = [];
        } else {
          listtemp = this.data.testEssence;
        }
        listtemp = listtemp.concat(res.data.rows.map(item => {
          item.title = item.title.replace(" ","&nbsp;");
          return item;
        }));
        this.setData({
          testEssence: listtemp
        })
      }
      wx.stopPullDownRefresh()
    })
  },

  toKdDetail(e) {
    var obj = e.currentTarget.dataset.obj
    if (obj.allowTry === "是") {
      wx.navigateTo({
        url: './detail/detail?id=' + obj.id,
      })
    } else {
      if (!app.islogin()) {
        wx.navigateTo({
          url: '../login/login',
        })
        return;
      }
      wx.navigateTo({
        url: '../buy/buy?type=1',
      })
    }
  },

  toExercise(e) {
    if (!app.islogin()) {
      wx.navigateTo({
        url: '../login/login',
      })
      return;
    }
    let index = e.currentTarget.dataset.index;
    if (index === 0) {
      wx.navigateTo({
        url: '../zjlx/zjlx?type=01',
      })
    } else if (index === 1) {
      wx.navigateTo({
        url: '../zjlx/zjlx?type=02',
      })
      // wx.navigateTo({
      //   url: '../exerciselist/exerciselist?type=02',
      // })
    } else if (index === 2) {
      wx.navigateTo({
        url: '../zjlx/zjlx?type=03',
      })

      // wx.navigateTo({
      //   url: '../exerciselist/exerciselist?type=03',
      // })
    } else if (index === 3) {
      wx.navigateTo({
        url: '../monikaochang/monikaochangstart/monikaochangstart',
      })
    } else if (index == 4) {
      wx.navigateTo({
        url: '../kqmystart/kqmystart',
      })
    } else if (index == 5) {
      wx.navigateTo({
        url: '../errorcement/errorcement?type=1',
      })
    } else if (index == 6) {
      wx.navigateTo({
        url: '../errorcement/errorcement?type=2',
      })
    } else if(index == 7){
      wx.navigateTo({
        url: '../mynote/mynote',
      })
    }
  },

  /**
   * 点击继续做题
   */
  jixuzuoti() {
    this.startTest();
  },

  /**
   * 切换科目
   */
  selectSubject() {
    wx.navigateTo({
      url: '../selectsubject/selectsubject?type=1',
    })
  }
})