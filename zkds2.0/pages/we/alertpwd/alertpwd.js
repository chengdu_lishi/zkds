// pages/we/alertpwd/alertpwd.js
let app =getApp();
let utils =require('../../../utils/util.js')
let wcache =require('../../../utils/wcache.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentInput: 0,
    userBean :null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '修改密码',
    })
    new app.WeToast();
    this.setData({
      userBean: app.globalData.userBean()
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  yzmfocus() {
    this.setData({ currentInput: 1 })
  },

  pwdfocus() {
    this.setData({ currentInput: 2 })
  },

  nicknamefocus() {
    this.setData({ currentInput: 3 })
  },

  yzmInput(e) {
    this.jpwd = e.detail.value;
  },

  pwdInput(e) {
    this.npwd = e.detail.value;
  },

  nicknameInput(e) {
    this.cpwd = e.detail.value;
  },

  alertpwd(){
    if (utils.isEmpty(this.jpwd)) {
      this.wetoast.toast({
        title: "请输入旧密码"
      })
      return;
    }

    if (utils.isEmpty(this.npwd)) {
      this.wetoast.toast({
        title: "请输入新密码"
      })
      return;
    }

    if (utils.isEmpty(this.cpwd)) {
      this.wetoast.toast({
        title: "请输入确认密码"
      })
      return;
    }

    if (this.npwd !== this.cpwd){
      this.wetoast.toast({
        title: "两次密码不一致"
      })
      return;
    }

    app.httpUtils.postLoading(app.Api.MODIFYPASSWORD,{
      phone: this.data.userBean.phone,
      password: this.jpwd,
      oldPassword: this.npwd
    }).then(res => {
      if(res.code == '00'){
        wcache.clear()
        wx.reLaunch({
          url: '../../login/login?type=1',
        })
      }else{
        this.wetoast.toast({
          title: res.msg
        })
      }
    })
  }
})