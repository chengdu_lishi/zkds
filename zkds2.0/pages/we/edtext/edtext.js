// pages/we/edtext/edtext.js
var event = require("../../../utils/event.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    inputType: 0,
    inputValue: "",
    textAreaValue:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("options.text === undefined " + options.text )

    this.setData({
      inputType: options.type,
      textAreaValue: options.text == 'undefined' ? "" : options.text
    })
  },

  userInput(e){
    this.setData({
      inputValue: e.detail.value
    })
  },

  userAreaInput(e){ 
    this.setData({
      textAreaValue: e.detail.value
    })
  },
  save() { 
    var text ="";
    var that = this;
    if (this.data.inputType!=='2'){
      text = this.data.inputValue;
    }else{
      text = this.data.textAreaValue;
    }
    event.emit("editext", { "type": that.data.inputType, "text": text })
    wx.navigateBack({})
  }
})