// pages/course/course.js
let app = getApp();
let wcache = require('../../../utils/wcache.js')
let utils = require('../../../utils/util.js')
var rows = 10;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    myVideoList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '我的视频',
    })
    this.pageMy = 1,
    this.getMyVideoList();
  },


  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
      this.pageMy = 1;
      this.getMyVideoList();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
      this.pageMy++;
      this.getMyVideoList();
  },

  toVideo(e) {
    let id = e.currentTarget.dataset.obj.id
    wx.navigateTo({
      url: '/pages/course/video/video?id=' + id,
    })
  },

  getMyVideoList() {
    app.httpUtils.postLoading(app.Api.LISTMYVIDEO, {
      page: this.pageMy,
      rows: rows,
      uid: app.globalData.userBean().id,
    }).then(res => {
      wx.stopPullDownRefresh();
      if (res.code == '00') {
        let listtemp = [];
        if (this.pageMy === 1) {
          listtemp = [];
        } else {
          listtemp = this.data.myVideoList;
        }
        listtemp = listtemp.concat(res.data.rows.map(item => {
          item.image = app.Api.getImageUrl(item.image);
          return item;
        }));
        this.setData({
          myVideoList: listtemp
        })
      }
    })
  },
})