// pages/we/userinfo/userinfo.js
var event =require("../../../utils/event.js")
let app =getApp();
let wcache =require("../../../utils/wcache.js")
let utils =require("../../../utils/util.js")
Page({
  /**
   * 页面的初始数据
   */
  data: {
    imagePath: "", 
    sexarray: ["../../../images/ic_rb_check.png", "../../../images/ic_rb_normal.png"],
    userBean:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   wx.setNavigationBarTitle({
     title: '个人中心',
   }) 

   new app.WeToast();
   var userBean =  app.globalData.userBean();
    for(var item in userBean){
      if (utils.isEmpty(userBean[item])){
        userBean[item]="";
      }
    }

    this.setData({ 
      imagePath: app.Api.getImageUrl(userBean.headPic),
      userBean: userBean
      })
    event.on("editext",this,(data)=>{
        if(data.type === "1"){
          this.data.userBean.nickName = data.text;
        }else if(data.type==="2"){
          this.data.userBean.signature = data.text;
        }else if(data.type==="3"){
          this.data.userBean.email = data.text;
        }
        this.setData({ userBean: this.data.userBean})
        this.updateUserBean();
    })
  },

/**
 * 性别选择对话框
 */
  selectSex () {
    let that = this;
    wx.showActionSheet({
      itemList: ['男', '女'],
      success: function (res) {
        that.data.userBean.sex = res.tapIndex == 0 ?'男':'女'
        that.setData({ userBean: that.data.userBean })
        that.updateUserBean();
      },
      fail: function (res) {
        console.log(res.errMsg)
      }
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    event.remove("editext",this);
  },

  openPhoto: function () {
    let that = this;
    wx.chooseImage({
      count: 1, // 默认9  
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        that.setData({
          imagePath: res.tempFilePaths[0]
        })
        console.log(that.data.imagePath)
       that.uploadImage();
      }
    })
  },

  uploadImage(){
    var that = this;
    wx.showLoading({ title: '请稍等', })
      wx.uploadFile({
        url: app.Api.UPLOADHEADPIC,
        filePath: that.data.imagePath,
        name: 'file',
        header: {
          "Content-Type": "multipart/form-data"
        },
        success: function (res) {
          wx.hideLoading();
          var data = JSON.parse(res.data)
          if(data.code == "00"){
            that.data.userBean.headPic = data.data
            that.updateUserBean();
          }else{
            that.wetoast.toast({
              title: data.msg
            })
          }
        },
        fail :function(error){
          console.log(error)
          wx.hideLoading();
        }
      })
  },

  updateUserBean(){
    app.httpUtils.postLoading(app.Api.USERUPADATE,{
      nickName: this.data.userBean.nickName,
      headPic: this.data.userBean.headPic,
      sex: this.data.userBean.sex,
      signature: this.data.userBean.signature,
      email: this.data.userBean.email,
      id: this.data.userBean.id
    }).then(res=>{})
  },

  logout(){
    app.httpUtils.postLoading(app.Api.LOGOUT, {
      id: this.data.userBean.id
    }).then(res => { 
      if(res.code=='00'){
        wcache.clear()
        wx.reLaunch({
          url: '../../login/login?type=1',
        })
      }else{
        that.wetoast.toast({
          title: res.msg
        })
      }
    })
  }
})
