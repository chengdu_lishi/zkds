// pages/we/we.js
let app =getApp();
let wcache = require('../../utils/wcache.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imagePath:"",
    userBean:null,
    subjecName:"",
    videoCout:0,
    messageCount:0
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var course = wcache.get("course", null);
    var user = app.globalData.userBean();
    this.setData({ 
      subjecName: course.name,
      imagePath: app.Api.getImageUrl(user.headPic), userBean: user})
    this.getUserInfo();
    this.getMyVideoList();
    this.getMessageList();
  },

/**
 * 获取视频数量
 */
  getMyVideoList() {
    app.httpUtils.post(app.Api.LISTMYVIDEO, {
      page: 1,
      rows: 10000,
      uid: app.globalData.userBean().id,
    }).then(res => {
      if (res.code == '00') {
        this.setData({
          videoCout: res.data.rows.length
        })
      }
    })
  },

/**
 * 获取消息数量
 */
  getMessageList() {
    app.httpUtils.post(app.Api.LISTMESSAGE, {
      page: 1,
      rows: 10000,
      uid: app.globalData.userBean().id
    }).then(res => {
      if (res.code == '00') {
        this.setData({
          messageCount: res.data.root.length
        })
      }
    })
  },

  getUserInfo(){
    app.httpUtils.post(app.Api.GETUSERINFO,{
      uid: app.globalData.userBean().id
    }).then(res=>{
      if(res.code =='00'){
        var temp =  res.data;
        wcache.put('user', temp);
        this.setData({
          imagePath: app.Api.getImageUrl(temp.headPic),
          userBean: res.data
        })    
      }
    })
  },
  
})