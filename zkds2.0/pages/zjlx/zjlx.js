// pages/zjlx/zjlx.js
let app = getApp();
let wcache = require('../../utils/wcache.js')
let page = 1;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    modelist: [{ "name": "正常模式" }, { "name": "预览模式" }],
    testInfolist: [],
    isbuy: false,
    mode: 1,
    isRedo: 0 ,
    testId: "",
    actionSheetItems: ["顺序模式", "浏览模式", "组卷模式"]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.type = options.type;
    app.event.on("tryagain", this, data => {
      if (data == -1) {
        this.setData({ isRedo: 0, testId: "" })
      } else {
        this.setData({ isRedo: 1,testId : data})
      }
    })

    app.event.on("jiaojuan", this, data => {
      this.isShow = data;
    })
    
    if (this.type == '01'){
      wx.setNavigationBarTitle({
        title: "章节练习",
      })
    }else if(this.type == '02'){
      wx.setNavigationBarTitle({
        title: "预测演练",
      })
    }else if(this.type == '03'){
      wx.setNavigationBarTitle({
        title: "历年真题",
      })
    }
  },

  onUnload() {
    app.event.remove("tryagain",this);
    app.event.remove("jiaojuan", this);
  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    page = 1;
    let jisojusn = wcache.get("jiaojuan", null);
    if (jisojusn != null) {
      this.jiaojuan(jisojusn);
    } else {
      this.getZjlxList();
    }

    if (this.isShow) {
      this.isShow = false;
      wx.navigateTo({
        url: '/pages/zuoti/report/report',
      })
    }  
  },

  jiaojuan(json) {
    app.httpUtils.postJsonLoading(app.Api.FINISH, JSON.stringify(json)).then(res => {
      wcache.remove("jiaojuan");
      page = 1;
      this.getZjlxList();
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    page = 1;
    this.getZjlxList();
  },


  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    page++;
    this.getZjlxList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  getZjlxList() {
    app.httpUtils.postLoading(app.Api.LISTTESTINFO, {
      uid: app.globalData.userBean().id,
      courseInfoId: wcache.get("courseInfoId", ""),
      type: this.type,
      rows: 10,
      page: page,
    }).then(res => {
      if (res.code == '00') {
        let listtemp = [];
        if (page == 1) {
          listtemp = [];
          let isbuy = false;
          if (res.data.rows != null && res.data.rows.length > 0) {
            isbuy = res.data.rows[0].isBuy > 0
            this.setData({
              isbuy: isbuy
            })
          }
        } else {
          listtemp = this.data.testInfolist;
        }
        let v = res.data.rows.map(item => {
          let answerNumber = 0;
          if (item.children != null) {
            for (let i = 0; i < item.children.length; i++) {
              let obj = item.children[i];
              if (obj.answerNumber == undefined) {
                answerNumber += 0;
              } else {
                answerNumber += obj.answerNumber;
              }
            }
          }
          item.answerNumber = answerNumber
          return item;
        })
        listtemp = listtemp.concat(v);
        this.setData({
          testInfolist: listtemp
        })
        wx.stopPullDownRefresh();

        //再做一次
        if (this.data.isRedo == 1 ){
          this.startTest(this.data.testId)
        }  
      }
    })
  }
  ,
  startTest(id) {
    app.httpUtils.postLoading(app.Api.STARTTEST, {
      testId: id,
      model: this.data.mode,
      uid: app.globalData.userBean().id,
      isRedo: this.data.isRedo,
    }).then(res => {
      //还原数据
      this.setData({ isRedo: 0, testId: "" })
      if (res.code == "00") {
        if (res.data.questions.length > 0) {
          wcache.put("ti", res.data);
          wx.navigateTo({
            url: '../zuoti/zuoti?type=' + this.data.mode
          })
        } else {
          wx.showToast({
            title: "没有题目",
          })
        }
      } else {
        wx.showToast({
          title: res.msg,
        })
      }
    })
  },

  bindTest(e) {
    let id = e.currentTarget.dataset.id;
    let obj = e.currentTarget.dataset.obj;

    if (obj.questionNumber <= 0) {
      wx.showToast({
        title: "没有题目",
      })
      return;
    }

    if (obj.allowTry == "是" || this.data.isbuy) {
      this.startTest(id);
    } else {
      // wx.navigateTo({
      //   url: '../buy/buy?type=1',
      // })
      wx.navigateTo({
        url: '../courseactive/courseactive',
      })
    }
  }
  ,

  /**
   * 选择模式
   */
  selectMode() {
    wx.showActionSheet({
      itemList: this.data.actionSheetItems,
      success: res => {
        this.setData({
          mode: res.tapIndex + 1
        })
      }
    })
  },
  
  radioChange(e){
    console.log(e);
    this.setData({
      mode: e.detail.value
    })
  }
})