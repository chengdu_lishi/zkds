// pages/zuoti/answersheet/answersheet.js
let app = getApp();
let wcache = require('../../../utils/wcache.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    questions: null,
    testInfo: null,
    clientHeight: 1000,
    modeType: 1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    new app.WeToast();
    let test = wcache.get("ti", null);
    this.setData({
      modeType: options.type,
      questions: test.questions,
      testInfo: test
    })
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        console.log(res);
        let bottom = 45
        if (that.data.modeType == 2 || that.data.modeType == 4 || that.data.modeType == 5 || that.data.modeType == 6 || that.data.modeType == 7) {
          bottom = 0;
        }
        that.setData({
          clientHeight: res.windowHeight - 40 - bottom 
        });
      }
    });
  },

  toIndex(e) {
    let index = e.currentTarget.dataset.index;
    app.event.emit("toindex", index);
    wx.navigateBack({})
  },

  isFinsh() {
    for (let i = 0; i < this.data.questions.length; i++) {
      var q = this.data.questions[i];
      if (q.myAnswer == undefined) {
        return false;
      }
    }
    return true;
  },

  jiaojuan() {
    if (this.isFinsh()) {
      app.event.emit("jiaojuan", true)
      wx.navigateBack({
        delta:2
      })
    } else {
      wx.showModal({
        title: '确认交卷',
        content: '本次做题还有题目未作答',
        success: () => {
          app.event.emit("jiaojuan", true)
          wx.navigateBack({
            delta: 2
          })
        }
      })
    }
  }
})