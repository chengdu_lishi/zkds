// pages/zuoti/report/report.js
var interval;
var varName;
let wcache = require("../../../utils/wcache.js");
var ctx = wx.createCanvasContext('canvasArcCir');
let utils = require("../../../utils/util.js");
let app = getApp();


// 时间格式化输出，如11:03 25:19 每1s都会调用一次
function dateformat(micro_second) {
  // 总秒数
  var second = Math.floor(micro_second / 1000);
  // 天数
  var day = Math.floor(second / 3600 / 24);
  // 小时
  var hr = Math.floor(second / 3600 % 24);
  // 分钟
  var min = Math.floor(second / 60 % 60);
  // 秒
  var sec = Math.floor(second % 60);
  if (day < 1) {
    if (hr < 1) {
      return +min + ":" + sec + "";
    } else if (hr >= 1) {
      return hr + ":" + min + ":" + sec + "";
    }
  } else {
    return day + "天" + hr + ":" + min + ":" + sec + "";
  }
  return day + "天" + hr + ":" + min + ":" + sec + "";
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    clientHeight: 1000,
    questions: null,
    testInfo: null,
    totalScoce :0.0,
    accuracy: 0,
    errorNumber: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.y = 100;
    var that = this;
    app.event.emit("jiaojuan",false)
    this.showData();
  },

  showData() {
    let test = wcache.get("ti", null);
    var timestamp = new Date().getTime()/1000; //当前毫秒值
    let time = test.beginTime;
    var date = time.substr(0, 10)//2017-02-27
    var hour = time.substr(11, 2) == '00' ? 0 : time.substr(11, 2).replace(/\b(0+)/gi, "") 
    var minute = time.substr(14, 2) == '00' ? 0 : time.substr(14, 2).replace(/\b(0+)/gi, "")
    var second = time.substr(17, 2) == '00' ? 0 : time.substr(17, 2).replace(/\b(0+)/gi, "")
    var ddate = parseInt(new Date(date).getTime() / 1000) + parseInt(hour) * 3600 + parseInt(minute) * 60 + parseInt(second) - 28800//别问我为什么-28800，只能告诉你实践出真知

    this.setData({
      questions: test.questions,
      testInfo: test,
      clock: dateformat((timestamp-ddate)*1000),
      accuracy: this.getAccuracy(test.questions),
      errorNumber: this.getErrorNumber(test.questions),
      totalScoce: this.getTotalScoce(test.questions)
    })
  },

/**
 * 错误题书
 */
  getErrorNumber(questions) {
    var  count = 0;
    for (let i = 0; i < questions.length; i++) {
      var questionsBean = questions[i];
      if (questionsBean.type == ("问答题") || questionsBean.type == ("案例题") || questionsBean.type == ("填空题")) {
      } else {
        var  isCorrenct = questionsBean.myAnswer == questionsBean.answer;
        if (!isCorrenct) {
          count++;
        }
      }
    }
    return count;
  },

/** 总分数*/
  getTotalScoce(questions) {
    var num = 0;
    for (let i = 0; i < questions.length; i++) {
      var questionsBean = questions[i];
      if (questionsBean.type == ("问答题") || questionsBean.type == ("案例题") || questionsBean.type == ("填空题")) {
        num += questionsBean.questionScore;
      } else {
        var isCorrenct = questionsBean.myAnswer == questionsBean.answer;
        if (isCorrenct) {
          num += questionsBean.questionScore;
        }
      }
    }
    return num;
  },

  /**
   * 平均值
   */
  getAccuracy(questions) { 
    var number1 = 0;
    var correntNumber = 0;
    for (let i = 0; i < questions.length ;i++ ){
      var questionsBean = questions[i];
      if (questionsBean.myAnswer != undefined) {
        number1 = number1+1;
      }
      var isCorrenct =false;
      if (questionsBean.type == ("问答题") || questionsBean.type == ("案例题") || questionsBean.type == ("填空题")) {
        isCorrenct = true;
      } else {
        isCorrenct = questionsBean.myAnswer == (questionsBean.answer);
      }
      if (isCorrenct) {
        correntNumber++;
      }
    }
    if (number1 == 0) {
      return 0;
    }
    return (correntNumber * 100 / number1).toFixed(2);
  },

  drawCircle: function () {
    function drawArc(s, e) {
      ctx.setFillStyle('white');
      ctx.clearRect(0, 0, 200, 200);
      ctx.draw();
      var x = 100, y = this.y, radius = 96;
      ctx.setLineWidth(5);
      ctx.setStrokeStyle('#ea9216');
      ctx.setLineCap('round');
      ctx.beginPath();
      ctx.arc(x, y, radius, s, e, false);
      ctx.stroke()
      ctx.draw()
    }

    var step = 1, startAngle = 1.5 * Math.PI, endAngle = 0;
    var animation_interval = 1000, n = 60;
    endAngle = step * 2 * Math.PI / n + 1.5 * Math.PI;
    drawArc(startAngle, endAngle);

  },

  onReady: function () {
    //创建并返回绘图上下文context对象。
    var cxt_arc = wx.createCanvasContext('canvasCircle');
    cxt_arc.setLineWidth(6);
    cxt_arc.setStrokeStyle('#eaeaea');
    cxt_arc.setLineCap('round');
    cxt_arc.beginPath();
    cxt_arc.arc(100, 100, 96, 0, 2 * Math.PI, false);
    cxt_arc.stroke();
    cxt_arc.draw();
  },

  getErrorList() {
    let questions = this.data.questions;
    if (questions != null && questions.length > 0) {
      let questionslist = [];
      for (let i = 0; i < questions.length; i++) {
        let question = questions[i];
        if (!utils.isEmpty(question.myAnswer)) {
          let isCorrect = 0;
          if (question.type == '问答题' || question.type == '案例题' || question.type == '填空题') {
            isCorrect = 1;
          } else {
            if (question.myAnswer == question.answer) {
              isCorrect = 1;
            } else {
              isCorrect = 0;
              questionslist.push(question);
            }
          }
        } else {
          questionslist.push(question);
        }
      }
      return questionslist;
    }
  }

  /**
   * 错题解析
   */
  , erroranalysis() {
    let qustions = this.getErrorList();
    if (qustions.length == 0) {
      wx.showToast({
        title: '没有错题',
      })
      return;
    }
    let testinfo = utils.copyobj(this.data.testInfo);
    console.log(testinfo);
    testinfo.questions = qustions;
    wcache.put('ti', testinfo);
    wx.navigateTo({
      url: "../../zuoti/zuoti?type=4"
    })
  },

  allanalysis() {
    wcache.put('ti', this.data.testInfo);
    wx.navigateTo({
      url: "../../zuoti/zuoti?type=5"
    })
  }

  , tryagain() {
    wx.navigateBack({});
    app.event.emit("tryagain", this.data.testInfo.testId)
  },

  scroll(e){
    this.y -= e.detail.scrollTop
    // e.detail.scrollTop;
  }
})