const DOMAIN = 'https://admin.lszkds.com'
module.exports = {
  DOMAIN: DOMAIN,
  LISTCLASSIFY: DOMAIN + "/app/course/listClassify",//获取课程分类
  GETIMG: DOMAIN + "/sysimage/getImg?url=",//获取图片
  LISTCOURSEINFO: DOMAIN + "/app/course/listCourseInfo",//获取课程列表
  CREATEUSER: DOMAIN + "/app/user/create", //注册
  SENDCODE: DOMAIN + "/app/user/sendsms", // 发送短信
  SETPASSWORD: DOMAIN + "/app/user/setPassword", //忘记密码
  LOGIN: DOMAIN + "/app/user/loginByPC",//登录接口
  GETBANNER: DOMAIN + "/app/course/getBanner",//获取广告图
  GETLASTEXERCISE: DOMAIN + "/app/course/getLastExercise", //获取最近练题记录
  LISTTESTESSENCE: DOMAIN + "/app/course/listTestEssence",//获取考点精华列表
  LISTVIDEOCOURSEHTTP: DOMAIN + "/app/video/listVideoCourse",//课程
  LISTMYVIDEO: DOMAIN + "/app/video/listMyVideo",//查看我购买的视频
  LISTARTICLE: DOMAIN + "/app/article/listArticle",//考试咨询
  UPLOADHEADPIC: DOMAIN + "/app/user/uploadHeadPic",//上传头像
  USERUPADATE: DOMAIN + "/app/user/update",//更新用户
  GETUSERINFO: DOMAIN + "/app/user/getUserInfo",//获取用户信息
  MODIFYPASSWORD: DOMAIN + "/app/user/modifyPassword",//修改密码
  LOGOUT: DOMAIN + "/app/user/logout" ,//退出登录
  LISTCOUPON: DOMAIN + "/app/buy/listCoupon",//获取优惠券
  GETARTICLEDETAILS: DOMAIN + "/app/article/getArticleDetails",//考试详情
  LISTMESSAGE : DOMAIN + "/app/msg/listMessage",//消息列表
  SETREAD: DOMAIN + "/app/msg/setRead",
  ACTIVE: DOMAIN + "/app/buy/active",
  GETSYSTEMINFO: DOMAIN + "/app/system/getSystemInfo",//获取关于我们
  GETLAW: DOMAIN + "/app/system/getLaw",//法律法规
  GETUSERSCORE: DOMAIN + "/app/user/getUserScore",//学习统计
  GETGOODS: DOMAIN + "/app/buy/getGoods", //金币列表
  RECHARGEBYMICROMSG: DOMAIN + "/app/buy/rechargeByMicroMsgMini",//微信充值
  LISTBUYPACKAGE: DOMAIN + "/app/buy/listBuyPackage",//我的订单
  getMessageDetails : DOMAIN + "/app/msg/getMessageDetails",//消息详情
  GETTESTESSENCEDETAILS: DOMAIN + "/app/course/getTestEssenceDetails",//考点详情
  GETVIDEOCOURSEDETAILS: DOMAIN + "/app/video/getVideoCourseDetails",//获取视频详情
  ADDVIDEOLOG: DOMAIN + "/app/video/addVideoLog",
  GETLASTVIDEO: DOMAIN + "/app/video/getLastVideo",//获取最近播放视频
  BEGINWATCH: DOMAIN + "/app/video/beginWatch",//开始视频播放
  GETBUYDETAILS: DOMAIN + "/app/buy/getBuyDetails",//获取购买详情
  PAYMENT: DOMAIN + "/app/buy/payment",//购买
  LISTTESTINFO: DOMAIN + "/app/testinfo/listTestInfo",//获取章节/试卷列表
  STARTTEST: DOMAIN + "/app/testinfo/startTest",//开始连体
  FINISH: DOMAIN + "/app/testinfo/finish", //完成交卷
  GETEXERCISE: DOMAIN + "/app/testinfo/getExercise",//继续做题
  GETOPENTIME: DOMAIN + "/app/testinfo/getOpenTime",//是否开启考前密押
  STARTEXAM: DOMAIN + "/app/testinfo/startExam",//开始组卷
  LISTEXAM: DOMAIN + "/app/testinfo/listExam", // 组卷列表
  RESITEXAM: DOMAIN + "/app/testinfo/resitExam", //组卷重新做
  DELETEEXAM: DOMAIN + "/app/testinfo/deleteExam", //删除
  LISTERRORTEST: DOMAIN + "/app/question/listErrorTest", //错题列表
  LISTCOLLECTTEST: DOMAIN + "/app/question/listCollectTest",//收藏题目列表
  COUNTERROR: DOMAIN + "/app/question/countError",//错题数
  LISTCOLLECT: DOMAIN + "/app/question/listCollect",//收藏题目书
  LISTERROR: DOMAIN + "/app/question/listError", // 错题巩固
  ANSWERERROR: DOMAIN + "/app/question/answerError", //提交错题答案
  NOTELIST: DOMAIN +"/app/question/listNotes",//我的笔记
  COLLECTION: DOMAIN + "/app/question/collectOrCancel",//收藏和取消
  MAKENOTES: DOMAIN + "/app/question/makeNotes",//添加笔记
  SUBMITERROR: DOMAIN + "/app/question/submitError",//纠错
  GETERRORTYPE: DOMAIN +"/app/question/getErrorType",//错题列表
  getImageUrl(imagePath) {
    return this.GETIMG + encodeURI(imagePath);
  }
}